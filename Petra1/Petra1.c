#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>

struct	ACTUATEURS
{
	unsigned CP : 2;
	unsigned C1 : 1;
	unsigned C2 : 1;
	unsigned PV : 1;
	unsigned PA : 1;
	unsigned AA : 1;
	unsigned GA : 1;
};

union
{
	struct ACTUATEURS act ;
	unsigned char byte ;
} u_act;

struct 	CAPTEURS
{
	unsigned L1 : 1;
	unsigned L2 : 1;
	unsigned T  : 1; 
	unsigned S  : 1;
	unsigned CS : 1;
	unsigned AP : 1;
	unsigned PP : 1;
	unsigned DE : 1;	
};

union
{
	struct CAPTEURS capt ;
	unsigned char byte ;
} u_capt;

void displaySensors();
void Menu();

int main ()
{
	int fd_petra_in,fd_petra_out;
	char entree = '9';

	u_act.act.PV = 1;

	//Ouverture des fichiers du PETRA
	fd_petra_out = open( "/dev/actuateursPETRA",O_WRONLY);
	if (fd_petra_out == -1)
	{
		perror ("MAIN : Erreur ouverture PETRA_OUT");
		return 1;
	}
	else
		printf ("MAIN: PETRA_OUT opened\n");

	fd_petra_in = open( "/dev/capteursPETRA",O_RDONLY);
	if (fd_petra_in == -1)
	{
		perror ("MAIN : Erreur ouverture PETRA_IN");
		return 1;
	}
	else
		printf ("MAIN: PETRA_IN opened\n");
	
	//Mise à 0 du byte des actuateurs
	u_act.byte = 0x00 ;

	while(entree != 'q')
	{
		printf("\033[2J"); //supprimer ce qui est à l'écran
		
		read(fd_petra_in, &u_capt.byte,1);
		
		//Affichage des capteurs
		displaySensors();
		
		//Affichage du menu
		Menu();
		
		if(tcischars(0)>0)
		{
			entree = getchar();
			fflush(stdin);
			switch(entree)
			{
				case '0':
					u_act.act.CP = 0;
					break;
				case '1':
					u_act.act.CP = 1;
					break;
				case '2':
					u_act.act.CP = 2;
					break;
				case '3':
					u_act.act.CP = 3;
					break;
				case '4':
					u_act.act.C1 ^= 1;
					break;
				case '5':
					u_act.act.C2 ^= 1;
					break;
				case '6':
					u_act.act.PV ^= 1;
					break;
				case '7':
					u_act.act.PA ^= 1;
					break;
				case '8':
					u_act.act.AA ^= 1;
					break;
				case '9':
					u_act.act.GA ^= 1;
					break;
				case 'q':
					break;
			}
		}
		
		write(fd_petra_out, &u_act.byte, 1);
	}
	
	//Fermeture des fichiers
	close(fd_petra_in);
	close(fd_petra_out);

	return 0;
}
void displaySensors()
{
	printf("+----------------------+\n"
		   "| PETRA SENSORS STATES |\n"
		   "+----------------------+\n\n");

	printf("  L1 : %u    L2 : %u\n",u_capt.capt.L1,u_capt.capt.L2);
	printf("  T  : %u    S  : %u\n",u_capt.capt.T,u_capt.capt.S);
	printf("  CS : %u    AP : %u\n",u_capt.capt.CS,u_capt.capt.AP);
	printf("  PP : %u    DE : %u\n",u_capt.capt.PP,u_capt.capt.DE);
}
void Menu()
{
	printf("\nActuateur a activer : \n");
	printf("0 - Chariot 0\n");
	printf("1 - Chariot 1\n");
	printf("2 - Chariot 2\n");
	printf("3 - Chariot 3\n");
	printf("4 - Convoyeur 1\n");
	printf("5 - Convoyeur 2\n");
	printf("6 - Ventouse\n");
	printf("7 - Plongeur sur chariot\n");
	printf("8 - Arbre\n");
	printf("9 - Grapin\n");
	printf("q - Quittez\n>");
}

