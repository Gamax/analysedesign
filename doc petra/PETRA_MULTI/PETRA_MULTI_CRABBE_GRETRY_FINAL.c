#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#define crit1 1 //slot et encoche
#define crit2 0 //slot uniquement
#define crit3 0 //encoche uniquement
#define clear "\33[2J]"

struct	ACTUATEURS
{
	unsigned CP : 2; //Chariot
	unsigned C1 : 1; //Conv1
	unsigned C2 : 1; //Conv2
	unsigned PV : 1; //Ventouse
	unsigned PA : 1; //Truc sur le chariot
	unsigned AA : 1; //Arbre
	unsigned GA : 1; //Grapin sur Arbre
};
union
{
	struct ACTUATEURS act;
	unsigned char byte;
} u_act;

struct 	CAPTEURS
{
	unsigned L1 : 1; //Capteur de corner 1 (trou � 0)
	unsigned L2 : 1; //Capteur de corner 2 (trou � 0)
	unsigned T : 1;	 //Cable H
	unsigned S : 1;  //Capteur de slot (trou � 0)
	unsigned CS : 1; //Capteur de chariot stable (immobile � 0)
	unsigned AP : 1; //Capteur de position de l'arbre (Tapis 1 � 0)
	unsigned PP : 1; //Capteur de position du plongeur (haut � 0)
	unsigned DE : 1; //Capteur du dispenser (vide � 1)

					 /*                 
					 unsigned H1 : 1;
					 unsigned H2 : 1;
					 unsigned H3 : 1;
					 unsigned H4 : 1;
					 unsigned H5 : 1;
					 unsigned H6 : 1;
					 unsigned    : 2 ;
					 */
};
union
{
	struct CAPTEURS capt;
	unsigned char byte;
} u_capt;

struct PIECE
{
	int mauvais;
	int slot;
	int corner;
};

void * fctPiece(void *arg);
void * fctTimeSlot(void *arg);
void * fctTimeCorner(void *arg);

void LectureCapt();
void LectureAct();
void Ecriture();
void Capteur(struct PIECE Piece);
void Sleep(int, unsigned int);

int fd_petra_in, fd_petra_out;
pthread_t HandlePiece[4];
pthread_t HandleTimeSlot;
pthread_t HandleTimeCorner;

pthread_mutex_t mCapt;
pthread_mutex_t mChar;
pthread_mutex_t mGrap;
pthread_mutex_t mC1;
pthread_mutex_t mC2;
pthread_mutex_t mDE;

main()
{
	int rc, cpt;
	int Choix, Choix2;
	int Fini, Fini2;
	int i = 0;

	struct PIECE Piece;
	Piece.mauvais = 0;
	Piece.slot = 0;
	Piece.corner = 0;

	pthread_mutex_init(&mCapt, NULL);
	pthread_mutex_init(&mChar, NULL);
	pthread_mutex_init(&mGrap, NULL);
	pthread_mutex_init(&mC1, NULL);
	pthread_mutex_init(&mC2, NULL);
	pthread_mutex_init(&mDE, NULL);

	fd_petra_out = open("/dev/actuateursPETRA", O_WRONLY);
	if (fd_petra_in == -1)
	{
		perror("MAIN : Erreur ouverture PETRA_OUT");
		return 1;
	}
	else
		printf("MAIN: PETRA_OUT opened\n");


	fd_petra_in = open("/dev/capteursPETRA", O_RDONLY);
	if (fd_petra_in == -1)
	{
		perror("MAIN : Erreur ouverture PETRA_IN");
		return 1;
	}
	else
		printf("MAIN: PETRA_IN opened\n");


	Fini = 1;
	do
	{

		Choix = 0;
		printf("\n\n\n        1.   Capteur \r\n");
		printf("\n\n\n        2.   Activateur \r\n");
		printf("\n\n\n        3.   Cycle multipiece.\r\n");
		printf("\n\n\n        4.   Quitter \r\n");
		fflush(stdin);
		scanf("%d", &Choix);

		switch (Choix)
		{
		case 1: // CAPTEUR

			while (1)
			{
				Capteur(Piece);
				Sleep(1, 0);
			}
			break;

		case 2: // ACTIVATEUR

			read(fd_petra_out, &u_act.byte, 1);

			Fini2 = 1;
			do
			{

				Choix2 = 0;
				printf("\n\n\n        1. Conv1    \r\n");
				printf("\n\n\n        2. Conv2   \r\n");
				printf("\n\n\n        3. Ventouse   \r\n");
				printf("\n\n\n        4. Plongueur   \r\n");
				printf("\n\n\n        5. Arbre   \r\n");
				printf("\n\n\n        6. Grappin   \r\n");
				printf("\n\n\n        7. Chariot   \r\n");
				printf("\n\n\n        8. Quitter Menu Activateur\r\n");
				fflush(stdin);
				scanf("%d", &Choix2);

				switch (Choix2)
				{

				case 1: // Conv1
					system("clear");

					printf("Capteur Conv1 => ");

					printf("\n\nACTIVER");

					u_act.act.C1 = 1;

					Ecriture();

					Sleep(3, 0);

					system("clear");

					printf("Capteur Conv1 => ");

					printf("\n\nDESACTIVER");

					u_act.act.C1 = 0;

					Ecriture();

					Sleep(1, 0);

					break;

				case 2: // Conv2
					system("clear");

					printf("Capteur Conv2 => ");

					printf("\n\nACTIVER");

					u_act.act.C2 = 1;

					Ecriture();

					Sleep(3, 0);

					system("clear");

					printf("Capteur Conv2 => ");

					printf("\n\nDESACTIVER");

					u_act.act.C2 = 0;

					Ecriture();

					Sleep(1, 0);

					break;

				case 3: // Ventouse
					system("clear");

					printf("Capteur Ventouse => ");

					printf("\n\nACTIVER");

					u_act.act.PV = 1;

					Ecriture();

					Sleep(3, 0);

					system("clear");

					printf("Capteur Ventouse => ");

					printf("\n\nDESACTIVER");

					u_act.act.PV = 0;

					Ecriture();

					Sleep(1, 0);

					break;

				case 4: // Plongueur
					system("clear");

					printf("Capteur Plongueur => ");

					printf("\n\nACTIVER");

					u_act.act.PA = 1;

					Ecriture();

					Sleep(3, 0);

					system("clear");

					printf("Capteur Plongueur => ");

					printf("\n\nDESACTIVER");

					u_act.act.PA = 0;

					Ecriture();

					Sleep(1, 0);

					break;

				case 5: // Arbre
					system("clear");

					printf("Capteur Arbre => ");

					printf("\n\nACTIVER");

					u_act.act.AA = 1;

					Ecriture();

					Sleep(3, 0);

					system("clear");

					printf("Capteur Arbre => ");

					printf("\n\nDESACTIVER");

					u_act.act.AA = 0;

					Ecriture();

					Sleep(1, 0);

					break;

				case 6: // Grappin
					system("clear");

					printf("Capteur Grappin => ");

					printf("\n\nACTIVER");

					u_act.act.GA = 1;

					Ecriture();

					Sleep(3, 0);

					system("clear");

					printf("Capteur Grappin => ");

					printf("\n\nDESACTIVER");

					u_act.act.GA = 1;
					Ecriture();

					Sleep(1, 0);

					break;

				case 7: // Chariot
					system("clear");

					printf("Capteur Chariot => ");

					printf("\n\nPosition 0");

					u_act.act.CP = 00;

					Ecriture();

					Sleep(4, 0);

					system("clear");

					printf("Capteur Chariot => ");

					printf("\n\nPosition 1");

					u_act.act.CP = 1;

					Ecriture();

					Sleep(4, 0);

					system("clear");

					printf("Capteur Chariot => ");

					printf("\n\nPosition 2");

					u_act.act.CP = 2;

					Ecriture();

					Sleep(4, 0);

					system("clear");

					printf("Capteur Chariot => ");

					printf("\n\nPosition 3");

					u_act.act.CP = 3;

					Ecriture();

					Sleep(4, 0);

					system("clear");

					printf("Capteur Chariot => ");

					printf("\n\nPosition 0");

					u_act.act.CP = 0;

					Ecriture();

					Sleep(4, 0);

					break;

				case 8: // QUITTER
					Fini2 = 0;
					break;
				}
			} while (Fini2);

			break;

		case 3:
			//D�sactivation de la ventouse
			u_act.act.PV = 0;
			Ecriture();
			//D�sactivation du plongeur
			u_act.act.PA = 0;
			Ecriture();
			//activation des tapis
			u_act.act.C1 = 1;
			u_act.act.C2 = 1;
			Ecriture();
			//remise de l'arbre en position de base
			u_act.act.AA = 0;
			Ecriture();
			//Desactivation du grappin
			u_act.act.GA = 0;
			Ecriture();
			//Remise du chariot en position 0
			u_act.act.CP = 0;
			Ecriture();
			Sleep(2, 0);
			printf("Fin init\n");

			pthread_create(&HandlePiece[0], NULL, fctPiece, 0);
			//Sleep(3, 0);
			printf("Thread 1 cree\n");

			pthread_create(&HandlePiece[1], NULL, fctPiece, 0);
			//Sleep(3, 0);
			printf("Thread 2 cree\n");

			pthread_create(&HandlePiece[2], NULL, fctPiece, 0);
			//Sleep(3, 0);
			printf("Thread 3 cree\n");

			pthread_create(&HandlePiece[3], NULL, fctPiece, 0);
			printf("Thread 4 cree\n");
			
			
			printf("TID principal: %d\n", pthread_self());
			while(1);
			break;

		case 4: // QUITTER
			Fini = 0;
			break;
		}
	} while (Fini);

	u_act.byte = 0x00;
	Ecriture();

	close(fd_petra_in);
	close(fd_petra_out);
}

void * fctPiece(void *arg)
{
	int GrapLock = 0, C1Lock = 0, CharLock = 0, ret = 0;

	struct PIECE Piece;

	Piece.mauvais = 0;
	Piece.slot = 0;
	Piece.corner = 0;

	do
	{
		printf(" Piece lancee\n");
		pthread_mutex_lock(&mDE);
		do
		{
			LectureCapt(); //Lecture des capteurs
			//printf(clear);
			printf(" vide\n");
		} while (u_capt.capt.DE); //Verification du stock
		pthread_mutex_unlock(&mDE);

		printf("TID: %d\n", pthread_self());
		pthread_mutex_lock(&mChar);
		LectureAct();
		u_act.act.PA = 1;  		//Descente du Plongeur							  	 	 
		Ecriture();

		Capteur(Piece);

		Sleep(1, 0);

		do
		{
			LectureCapt(); //attend que le plongeur soit en bas
		} while (!u_capt.capt.PP);

		Capteur(Piece);

		LectureAct();
		u_act.act.PV = 1;  		//Ventouse activation				  	 	 
		Ecriture();

		Capteur(Piece);

		LectureAct();
		u_act.act.PA = 0;  		//Remont�e du Plongeur						  	 	 
		Ecriture();

		Capteur(Piece);

		do
		{
			LectureCapt(); //attend que le plongeur soit en haut
		} while (u_capt.capt.PP);

		LectureAct();
		u_act.act.CP = 1;	    //Chariot en position 1  				        		 
		Ecriture();

		Capteur(Piece);

		do
		{
			LectureCapt(); //boucle tant que le chariot ne se d�place pas
		} while (!u_capt.capt.CS);

		Capteur(Piece);

		do
		{
			LectureCapt(); //boucle tant que le chariot se d�place
		} while (u_capt.capt.CS);

		Capteur(Piece);

		LectureAct();
		u_act.act.PV = 0;  		//Ventouse d�sactivation
		Ecriture();

		Capteur(Piece);

		LectureAct();
		u_act.act.CP = 0;	    //Chariot en position 0  				        		 
		Ecriture();

		Capteur(Piece);

		do
		{
			LectureCapt(); //boucle tant que le chariot ne se d�place pas
		} while (!u_capt.capt.CS);

		Capteur(Piece);

		do
		{
			LectureCapt(); //boucle tant que le chariot se d�place
		} while (u_capt.capt.CS);

		pthread_mutex_unlock(&mChar);
		Capteur(Piece);

		pthread_create(&HandleTimeSlot, NULL, fctTimeSlot, (void*)pthread_self());

		do
		{
			LectureCapt(); //boucle tant qu'il n'y a pas de mati�re
		} while (!u_capt.capt.S);

		if (ret == pthread_cancel(HandleTimeSlot))
			printf("Timeout desamorce\n");
		Capteur(Piece);

		do
		{
			LectureCapt(); //boucle tant qu'il y a de la mati�re
		} while (u_capt.capt.S);

		Capteur(Piece);

		pthread_mutex_lock(&mC1);
		Sleep(1, 0);
		pthread_mutex_unlock(&mC1);

		LectureCapt();
		if (u_capt.capt.S)
		{
			//Il y a un slot
			Piece.slot = 1;
			if (crit3)
				Piece.mauvais = 1;
			Capteur(Piece);
			pthread_mutex_lock(&mC1);
			Sleep(1, 0);
			pthread_mutex_unlock(&mC1);
		}
		else
		{
			//Il n'y a pas de slot
			if (crit1 || crit2)
				Piece.mauvais = 1;
			Capteur(Piece);
			pthread_mutex_lock(&mC1);
			Sleep(1, 0);
			pthread_mutex_unlock(&mC1);
		}

		pthread_mutex_lock(&mGrap);
		LectureAct();
		u_act.act.GA = 1;			//Activation du grappin
		Ecriture();

		Capteur(Piece);

		LectureAct();			
		u_act.act.AA = 1;			//mise de l'arbre en position 1
		Ecriture();

		Capteur(Piece);

		do
		{
			LectureCapt(); //boucle tant que l'arbre n'est pas au tapis 2
		} while (!u_capt.capt.AP);

		Capteur(Piece);
		Sleep(0, 500000000);

		pthread_mutex_lock(&mC2);
		LectureAct();
		u_act.act.GA = 0;			//D�sactivation du grappin
		Ecriture();
		pthread_mutex_unlock(&mC2);

		Capteur(Piece);
		Sleep(0, 500000000);

		LectureAct();
		u_act.act.AA = 0;				//mise de l'arbre en position 0
		Ecriture();
		pthread_mutex_unlock(&mGrap);

		Capteur(Piece);

		pthread_create(&HandleTimeCorner, NULL, fctTimeCorner, (void*)pthread_self());

		do
		{
			LectureCapt(); //boucle tant que le second capteur du corner ne d�tecte rien
		} while (!u_capt.capt.L2);

		if(ret == pthread_cancel(HandleTimeCorner))
			printf("Timeout desamorce");
		Capteur(Piece);

		if (u_capt.capt.L1)
		{
			//Il n'y a pas d'encoche
			if (crit1 || crit3)
				Piece.mauvais = 1;
			Capteur(Piece);
		}
		else
		{
			Piece.corner = 1;
			//il y a une encoche
			if (crit2)
				Piece.mauvais = 1;
			Capteur(Piece);
		}

		if (Piece.mauvais)
		{
			if (pthread_mutex_trylock(&mC1) == 0)
			{
				C1Lock = 1;

				LectureAct();
				u_act.act.C1 = 0;			//Tapis 1 d�sactivation
				Ecriture();
			}

			if (pthread_mutex_trylock(&mChar) == 0)
			{
				CharLock = 1;

				LectureAct();
				u_act.act.CP = 3;			//Chariot en position 3
				Ecriture();
			}

			if (Piece.slot)
			{
				Sleep(2, 95000000);
			}
			else
			{
				Sleep(3, 15000000);
			}

			pthread_mutex_lock(&mC2);
			LectureAct();
			u_act.act.C2 = 0;				//Tapis 2 d�sactivation
			Ecriture();

			if (C1Lock == 0)
			{
				pthread_mutex_lock(&mC1);

				LectureAct();
				u_act.act.C1 = 0;			//Tapis 1 d�sactivation
				Ecriture();
			}

			if (CharLock == 0)
			{
				pthread_mutex_lock(&mChar);

				LectureAct();
				u_act.act.CP = 3;			//Chariot en position 3
				Ecriture();
			}

			Capteur(Piece);
			GrapLock = 0;
			C1Lock = 0;
			CharLock = 0;

			do
			{
				LectureCapt(); //boucle tant que le chariot ne se d�place pas
			} while (!u_capt.capt.CS);

			Sleep(1, 0);
			Capteur(Piece);

			do
			{
				LectureCapt(); //boucle tant que le chariot se d�place
			} while (u_capt.capt.CS);

			Capteur(Piece);

			LectureAct();
			u_act.act.PA = 1;  		//Descente du Plongeur							  	 	 
			Ecriture();

			Capteur(Piece);

			Sleep(1, 0);

			do
			{
				LectureCapt(); //attend que le plongeur soit en bas
			} while (!u_capt.capt.PP);

			Capteur(Piece);

			LectureAct();
			u_act.act.PV = 1;  		//Ventouse activation				  	 	 
			Ecriture();

			Capteur(Piece);

			LectureAct();
			u_act.act.PA = 0;  		//Remont�e du Plongeur						  	 	 
			Ecriture();

			Capteur(Piece);

			do
			{
				LectureCapt(); //attend que le plongeur soit en haut
			} while (u_capt.capt.PP);

			Capteur(Piece);

			LectureAct();
			u_act.act.CP = 2;		//Chariot en position 2
			Ecriture();

			do
			{
				LectureCapt(); //boucle tant que le chariot ne se d�place pas
			} while (!u_capt.capt.CS);

			Capteur(Piece);
			Sleep(1, 0);

			do
			{
				LectureCapt(); //boucle tant que le chariot se d�place
			} while (u_capt.capt.CS);

			Capteur(Piece);

			LectureAct();
			u_act.act.PA = 1;  		//Descente du Plongeur							  	 	 
			Ecriture();

			Capteur(Piece);

			Sleep(1, 0);

			do
			{
				LectureCapt(); //attend que le plongeur soit en bas
			} while (!u_capt.capt.PP);

			Capteur(Piece);

			LectureAct();
			u_act.act.PV = 0;  		//Ventouse d�sactivation				  	 	 
			Ecriture(); 

			Capteur(Piece);

			LectureAct();
			u_act.act.PA = 0;  		//Remont�e du Plongeur						  	 	 
			Ecriture();

			Capteur(Piece);

			do
			{
				LectureCapt(); //attend que le plongeur soit en haut
			} while (u_capt.capt.PP);

			Capteur(Piece);

			LectureAct();
			u_act.act.CP = 0;	    //Chariot en position 0 				        		 
			Ecriture();

			Capteur(Piece);

			do
			{
				LectureCapt(); //boucle tant que le chariot ne se d�place pas
			} while (!u_capt.capt.CS);

			Capteur(Piece);

			do
			{
				LectureCapt(); //boucle tant que le chariot se d�place
			} while (u_capt.capt.CS);

			LectureAct();
			u_act.act.C1 = 1;			//Tapis 1 activation
			u_act.act.C2 = 1;			//Tapis 2 activation
			Ecriture();
			pthread_mutex_unlock(&mC2);
			pthread_mutex_unlock(&mC1);
			pthread_mutex_unlock(&mChar);
			Capteur(Piece);
		}

	}while (1);
}

void * fctTimeSlot(void *arg)
{
	int ret = 0;
	pthread_t TIDPiece = (pthread_t)arg;
	Sleep(8, 0);

	printf("TIDPIECE: %d\n", TIDPiece);
	pthread_mutex_lock(&mCapt);
	printf("mCapt taken\n");
	if (ret == pthread_cancel(TIDPiece));
		printf("Thread desamorce: %d\n", ret);
	pthread_mutex_unlock(&mCapt);
	printf("mCapt released\n");

	printf("TIMEOUT DETECTE\r\n");
	pthread_create(&HandlePiece[0], NULL, fctPiece, 0);
}

void * fctTimeCorner(void *arg)
{
	int ret = 0;
	pthread_t TIDPiece = (pthread_t)arg;
	Sleep(6, 0);

	printf("TIDPIECE: %d\n", TIDPiece);
	pthread_mutex_lock(&mCapt);
	printf("mCapt taken\n");
	if (ret == pthread_cancel(TIDPiece));
		printf("Thread desamorce: %d\n", ret);
	pthread_mutex_unlock(&mCapt);
	printf("mCapt released\n");

	printf("TIMEOUT DETECTE\r\n");
	pthread_create(&HandlePiece[0], NULL, fctPiece, 0);
}

void LectureCapt()
{
	//pthread_mutex_lock(&mCapt);
	read(fd_petra_in, &u_capt.byte, 1);
	//pthread_mutex_unlock(&mCapt);
}

void LectureAct()
{
	read(fd_petra_out, &u_act.byte, 1);
}

void Ecriture()
{
	write(fd_petra_out, &u_act.byte, 1);
}

void Capteur(struct PIECE Piece)
{
	/*printf(clear);
	if (crit1)
	{
		printf("Critere : Un slot et une encoche requis.\r\n");
	}
	else
	{
		if (crit2)
		{
			printf("Critere : Un slot uniquement.\r\n");
		}
		else
		{
			if (crit3)
			{
				printf("Critere : une encoche uniquement.\r\n");
			}
			else
			{
				printf("Critere : Aucun selectionner.\r\n");
			}
		}
	}
	if (Piece.slot)
	{
		printf("SLOT CENTRAL DETECTE\r\n");
	}
	if (Piece.corner)
	{
		printf("ENCOCHE DETECTE\r\n");
	}
	if (Piece.mauvais)
	{
		printf("MAUVAISE PIECE\r\n");
	}

	LectureCapt();

	printf("\n");
	printf("CAP Stock			= %d \r\n", u_capt.capt.DE);
	printf("CAP Ch. instable		= %d \r\n", u_capt.capt.CS);
	printf("CAP Plongueur			= %d \r\n", u_capt.capt.PP);
	printf("CAP Slot			= %d \r\n", u_capt.capt.S);
	printf("CAP Cut Out			= %d \r\n", u_capt.capt.L1);
	printf("CAP Cut Out			= %d \r\n", u_capt.capt.L2);
	printf("CAP Arbre			= %d \r\n", u_capt.capt.AP);*/
}

void Sleep(int sec, unsigned int nsec)
{
	struct timespec time_id;
	time_id.tv_sec = sec;
	time_id.tv_nsec = nsec;

	nanosleep(&time_id, NULL);
}
