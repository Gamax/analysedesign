#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>

#define crit1 1 //slot et encoche
#define crit2 0 //slot uniquement
#define crit3 0 //encoche uniquement

#define clear "\33[2J]"


struct	ACTUATEURS
		{
                 unsigned CP : 2; //Chariot
                 unsigned C1 : 1; //Conv1
                 unsigned C2 : 1; //Conv2
                 unsigned PV : 1; //Ventouse
                 unsigned PA : 1; //Truc sur le chariot
                 unsigned AA : 1; //Arbre
                 unsigned GA : 1; //Grapin sur Arbre
        };
union
	{
		struct ACTUATEURS act ;
		unsigned char byte ;
	} u_act ;

void Capteur();
void Sleep(int,int);

struct 	CAPTEURS
		{
			 unsigned L1 : 1; //Capteur de corner 1 (trou � 0)
			 unsigned L2 : 1; //Capteur de corner 2 (trou � 0)
			 unsigned T  : 1; /* cable H */
			 unsigned S  : 1; //Capteur de slot (trou � 0)
			 unsigned CS : 1; //Capteur de chariot stable (immobile � 0)
			 unsigned AP : 1; //Capteur de position de l'arbre (Tapis 1 � 0)
			 unsigned PP : 1; //Capteur de position du plongeur (haut � 0)
			 unsigned DE : 1; //Capteur du dispenser (vide � 1)
		};

union
	{
		struct CAPTEURS capt ;
		unsigned char byte ;
	} u_capt ;


struct timespec time_id;
int fd_petra_in, fd_petra_out ;
int mauvais=0,slot=0,corner=0;

main ()
{
	int rc, cpt;
	int Choix,Choix2;
	int Fini,Fini2;
	int tour=3,init=0;

	fd_petra_out = open ( "/dev/actuateursPETRA", O_WRONLY );
	if ( fd_petra_in == -1 )
	{
		perror ( "MAIN : Erreur ouverture PETRA_OUT" );
		return 1;
	}
	else
		printf ("MAIN: PETRA_OUT opened\n");


	fd_petra_in = open ( "/dev/capteursPETRA", O_RDONLY );
	if ( fd_petra_in == -1 )
	{
		perror ( "MAIN : Erreur ouverture PETRA_IN" );
		return 1;
	}
	else
		printf ("MAIN: PETRA_IN opened\n");


	Fini=1;
	do
	{

		Choix=0;
		printf ( "\n\n\n        1.   Capteur \r\n" );
		printf ( "\n\n\n        2.   Activateur \r\n" );
		printf ( "\n\n\n        3.   Cycle monopiece.\r\n" );
		printf ( "\n\n\n        4.   Quitter \r\n" );
		fflush(stdin);
		scanf("%d",&Choix);

		switch(Choix)
		{
			case 1 : // CAPTEUR

				while(1)
				{
	                Capteur();
					Sleep ( 1,0 );
				}
							break;

			case 2 : // ACTIVATEUR

							read ( fd_petra_out , &u_act.byte , 1 );

							Fini2=1;
							do
							{

								Choix2=0;
								printf ( "\n\n\n        1. Conv1    \r\n" );
								printf ( "\n\n\n        2. Conv2   \r\n" );
								printf ( "\n\n\n        3. Ventouse   \r\n" );
								printf ( "\n\n\n        4. Plongueur   \r\n" );
								printf ( "\n\n\n        5. Arbre   \r\n" );
								printf ( "\n\n\n        6. Grappin   \r\n" );
								printf ( "\n\n\n        7. Chariot   \r\n" );
								printf ( "\n\n\n        8. Quitter Menu Activateur\r\n" );
								fflush(stdin);
								scanf("%d",&Choix2);

								switch(Choix2)
								{

									case 1 : // Conv1
	       				        		 system("clear");

	       				        		 printf("Capteur Conv1 => ");

	       				        		 printf("\n\nACTIVER");

	       				        		 u_act.act.C1=1;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 3,0 );

	       				        		 system("clear");

	       				        		 printf("Capteur Conv1 => ");

    									  	 	 printf("\n\nDESACTIVER");

    									  	 	 u_act.act.C1=0;

    									  	 	 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 1,0 );

													break;

									case 2 : // Conv2
	       				        		 system("clear");

	       				        		 printf("Capteur Conv2 => ");

	       				        		 printf("\n\nACTIVER");

	       				        		 u_act.act.C2=1;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 3,0 );

	       				        		 system("clear");

	       				        		 printf("Capteur Conv2 => ");

    									  	 	 printf("\n\nDESACTIVER");

    									  	 	 u_act.act.C2=0;

    									  	 	 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 1,0 );

													break;

									case 3 : // Ventouse
	       				        		 system("clear");

	       				        		 printf("Capteur Ventouse => ");

	       				        		 printf("\n\nACTIVER");

	       				        		 u_act.act.PV=1;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 3,0 );

	       				        		 system("clear");

	       				        		 printf("Capteur Ventouse => ");

    									  	 	 printf("\n\nDESACTIVER");

    									  	 	 u_act.act.PV=0;

    									  	 	 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 1,0 );

													break;

									case 4 : // Plongueur
	       				        		 system("clear");

	       				        		 printf("Capteur Plongueur => ");

	       				        		 printf("\n\nACTIVER");

	       				        		 u_act.act.PA=1;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 3,0 );

	       				        		 system("clear");

	       				        		 printf("Capteur Plongueur => ");

    									  	 	 printf("\n\nDESACTIVER");

    									  	 	 u_act.act.PA=0;

    									  	 	 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 1,0 );

													break;

									case 5 : // Arbre
	       				        		 system("clear");

	       				        		 printf("Capteur Arbre => ");

	       				        		 printf("\n\nACTIVER");

	       				        		 u_act.act.AA=1;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 3,0 );

	       				        		 system("clear");

	       				        		 printf("Capteur Arbre => ");

    									  	 	 printf("\n\nDESACTIVER");

    									  	 	 u_act.act.AA=0;

    									  	 	 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 1,0 );

													break;

									case 6 : // Grappin
	       				        		 system("clear");

	       				        		 printf("Capteur Grappin => ");

	       				        		 printf("\n\nACTIVER");

	       				        		 u_act.act.GA=1;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 3,0 );

	       				        		 system("clear");

	       				        		 printf("Capteur Grappin => ");

    									  	 	 printf("\n\nDESACTIVER");

    									  	 	 u_act.act.GA=1;
    									  	 	 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 1,0 );

													break;

									case 7 : // Chariot
	       				        		 system("clear");

	       				        		 printf("Capteur Chariot => ");

	       				        		 printf("\n\nPosition 0");

	       				        		 u_act.act.CP=00;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 4,0 );

    									  	 	 system("clear");

	       				        		 printf("Capteur Chariot => ");

	       				        		 printf("\n\nPosition 1");

	       				        		 u_act.act.CP=1;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 4,0 );

    									  	 	 system("clear");

	       				        		 printf("Capteur Chariot => ");

	       				        		 printf("\n\nPosition 2");

	       				        		 u_act.act.CP=2;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 4,0 );

    									  	 	 system("clear");

	       				        		 printf("Capteur Chariot => ");

	       				        		 printf("\n\nPosition 3");

	       				        		 u_act.act.CP=3;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 4,0 );

    									  	 	 system("clear");

	       				        		 printf("Capteur Chariot => ");

	       				        		 printf("\n\nPosition 0");

	       				        		 u_act.act.CP=0;

	       				        		 write ( fd_petra_out , &u_act.byte ,1 );

    									  	 	 Sleep ( 4,0 );

													break;

										case 8 : // QUITTER
											Fini2=0;
													break;
								}
							}while(Fini2);

							break;
			case 3 :
					while(tour>0)
					{
						mauvais=slot=corner=0;
						if(!init)
						{
							//D�sactivation de la ventouse
							u_act.act.PV=0;
    						write ( fd_petra_out , &u_act.byte ,1 );
							//D�sactivation du plongeur
							u_act.act.PA=0;
    						write ( fd_petra_out , &u_act.byte ,1 );
							//D�sactivation des tapis
							u_act.act.C1=0;
							u_act.act.C2=0;
							write ( fd_petra_out , &u_act.byte ,1 );
							//remise de l'arbre en position de base
							u_act.act.AA=0;
							write ( fd_petra_out , &u_act.byte ,1 );
							//Desactivation du grappin
							u_act.act.GA=0;
    						write ( fd_petra_out , &u_act.byte ,1 );
    						//Remise du chariot en position 0
							u_act.act.CP=0;
	       					write ( fd_petra_out , &u_act.byte ,1 );
							Sleep(2,0);
							init++;
						}

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //Lecture des capteurs
							printf(" vide\n");
						}while(u_capt.capt.DE); //Verification du stock

						u_act.act.PA=1;  		//Descente du Plongeur
    					write ( fd_petra_out , &u_act.byte ,1 );

						Capteur();

						Sleep(1,0);

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //attend que le plongeur soit en bas
						}while(!u_capt.capt.PP);

						Capteur();

						u_act.act.PV=1;  		//Ventouse activation
    					write ( fd_petra_out , &u_act.byte ,1 );

						Capteur();

						u_act.act.PA=0;  		//Remont�e du Plongeur
    					write ( fd_petra_out , &u_act.byte ,1 );

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //attend que le plongeur soit en haut
						}while(u_capt.capt.PP);

						u_act.act.CP=1;	    //Chariot en position 1
	       				write ( fd_petra_out , &u_act.byte ,1 );

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que le chariot ne se d�place pas
						}while(!u_capt.capt.CS);

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que le chariot se d�place
						}while(u_capt.capt.CS);

						Capteur();

						u_act.act.PV=0;  		//Ventouse d�sactivation

						u_act.act.C1=1;			//Tapis 1 activation
						write ( fd_petra_out , &u_act.byte ,1 );

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant qu'il n'y a pas de mati�re
						}while(!u_capt.capt.S);

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant qu'il y a de la mati�re
						}while(u_capt.capt.S);

						Capteur();

						Sleep(1,0);

						read ( fd_petra_in , &u_capt.byte , 1 );

						if(u_capt.capt.S)
						{
							//Il y a un slot
							slot = 1;
							if(crit3)
								mauvais=1;
							Capteur();
							Sleep(2,0);
						}
						else
						{
							//Il n'y a pas de slot
							if(crit1 || crit2)
								mauvais=1;
							Capteur();
							Sleep(1,0);
						}

						//Activation du grappin
						u_act.act.GA=1;
    					write ( fd_petra_out , &u_act.byte ,1 );

						Capteur();

						u_act.act.C1=0;			//Tapis 1 d�sactivation

						//mise de l'arbre en position 1
						u_act.act.AA=1;
						write ( fd_petra_out , &u_act.byte ,1 );

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que l'arbre n'est pas au tapis 2
						}while(!u_capt.capt.AP);

						Capteur();
						Sleep(1,0);

						//D�sactivation du grappin
						u_act.act.GA=0;

						u_act.act.C2=1;		//Tapis 2 activation
						write ( fd_petra_out , &u_act.byte ,1 );


						Capteur();
						Sleep(1,0);

						//mise de l'arbre en position 0
						u_act.act.AA=0;
						write ( fd_petra_out , &u_act.byte ,1 );

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que le second capteur du corner ne d�tecte rien
						}while(!u_capt.capt.L2);

						Capteur();

						if(u_capt.capt.L1)
						{
							//Il n'y a pas d'encoche
							if(crit1 || crit3)
								mauvais = 1;
							Capteur();
						}
						else
						{
							corner=1;
							//il y a une encoche
							if(crit2)
								mauvais = 1;
							Capteur();
						}

						if(mauvais)
						{
							if(slot)
							{
								Sleep(2,95000000);
							}
							else
							{
								Sleep(3,15000000);
							}
							Capteur();
							u_act.act.C2=0;		//Tapis 2 d�sactivation
							u_act.act.CP=3;		//Chariot en position 3
							write ( fd_petra_out , &u_act.byte ,1 );
							Capteur();

							do
							{
								read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que le chariot ne se d�place pas
							}while(!u_capt.capt.CS);


							Sleep(1,0);
							Capteur();

							do
							{
								read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que le chariot se d�place
							}while(u_capt.capt.CS);


							Capteur();

							u_act.act.PA=1;  		//Descente du Plongeur
    						write ( fd_petra_out , &u_act.byte ,1 );

							Capteur();

							Sleep(1,0);

							do
							{
								read ( fd_petra_in , &u_capt.byte , 1 ); //attend que le plongeur soit en bas
							}while(!u_capt.capt.PP);

							Capteur();

							u_act.act.PV=1;  		//Ventouse activation
    						write ( fd_petra_out , &u_act.byte ,1 );

							Capteur();

							u_act.act.PA=0;  		//Remont�e du Plongeur
    						write ( fd_petra_out , &u_act.byte ,1 );

							Capteur();

							do
							{
								read ( fd_petra_in , &u_capt.byte , 1 ); //attend que le plongeur soit en haut
							}while(u_capt.capt.PP);

							Capteur();

							u_act.act.CP=2;		//Chariot en position 2
							write ( fd_petra_out , &u_act.byte ,1 );

							do
							{
								read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que le chariot ne se d�place pas
							}while(!u_capt.capt.CS);

							Capteur();
							Sleep(1,0);

							do
							{
								read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que le chariot se d�place
							}while(u_capt.capt.CS);

							Capteur();

							u_act.act.PA=1;  		//Descente du Plongeur
    						write ( fd_petra_out , &u_act.byte ,1 );

							Capteur();

							Sleep(1,0);

							do
							{
								read ( fd_petra_in , &u_capt.byte , 1 ); //attend que le plongeur soit en bas
							}while(!u_capt.capt.PP);

							Capteur();

							u_act.act.PV=0;  		//Ventouse d�sactivation
    						write ( fd_petra_out , &u_act.byte ,1 );

							Capteur();

							u_act.act.PA=0;  		//Remont�e du Plongeur
    						write ( fd_petra_out , &u_act.byte ,1 );

							Capteur();

							do
							{
								read ( fd_petra_in , &u_capt.byte , 1 ); //attend que le plongeur soit en haut
							}while(u_capt.capt.PP);

							Capteur();

						}
						else
						{
							Sleep(4,0);
							u_act.act.C2=0;			//Tapis 2 d�sactivation
							Capteur();
						}

						u_act.act.CP=0;	    //Chariot en position 0
		       			write ( fd_petra_out , &u_act.byte ,1 );

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que le chariot ne se d�place pas
						}while(!u_capt.capt.CS);

						Capteur();

						do
						{
							read ( fd_petra_in , &u_capt.byte , 1 ); //boucle tant que le chariot se d�place
						}while(u_capt.capt.CS);

						Capteur();

						tour--;

					}
				break;

			case 4 : // QUITTER
					Fini=0;
							break;
		}
	}while(Fini);

	u_act.byte = 0x00;
	write ( fd_petra_out , &u_act.byte ,1 );

	close ( fd_petra_in );
	close ( fd_petra_out );
}

void Capteur()
{
	printf(clear);
	if(crit1)
	{
		printf("Critere : Un slot et une encoche requis.\r\n");
	}
	else
	{
		if(crit2)
		{
			printf("Critere : Un slot uniquement.\r\n");
		}
		else
		{
			if(crit3)
			{
				printf("Critere : une encoche uniquement.\r\n");
			}
			else
			{
				printf("Critere : Aucun selectionne.\r\n");
			}
		}
	}
	if(slot)
	{
		printf("SLOT CENTRAL DETECTE\r\n");
	}
	if(corner)
	{
		printf("ENCOCHE DETECTE\r\n");
	}
	if(mauvais)
	{
		printf("MAUVAISE PIECE\r\n");
	}
	printf("\n");
	read ( fd_petra_in , &u_capt.byte , 1 );
	printf ( "CAP Stock          = %d \r\n" , u_capt.capt.DE );
	printf ( "CAP Ch. instable = %d \r\n" , u_capt.capt.CS );
	printf ( "CAP Plongueur   = %d \r\n" , u_capt.capt.PP );
	printf ( "CAP Slot             = %d \r\n" , u_capt.capt.S );
	printf ( "CAP Cut Out        = %d \r\n" , u_capt.capt.L1 );
	printf ( "CAP Cut Out        = %d \r\n" , u_capt.capt.L2 );
	printf ( "CAP Arbre           = %d \r\n" , u_capt.capt.AP );
}

void Sleep(int sec, int nsec)
{
	time_id.tv_sec=sec;
	time_id.tv_nsec=nsec;

	nanosleep(&time_id,NULL);
}
