﻿namespace Petra_client
{
    partial class GUIPetra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelL1 = new System.Windows.Forms.Label();
            this.labelL2 = new System.Windows.Forms.Label();
            this.labelT = new System.Windows.Forms.Label();
            this.labelS = new System.Windows.Forms.Label();
            this.labelCS = new System.Windows.Forms.Label();
            this.labelAP = new System.Windows.Forms.Label();
            this.labelPP = new System.Windows.Forms.Label();
            this.labelDE = new System.Windows.Forms.Label();
            this.textBoxCode = new System.Windows.Forms.TextBox();
            this.buttonExecute = new System.Windows.Forms.Button();
            this.buttonCheck = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.codeStatusLabel = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(236, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Port : ";
            // 
            // textBoxIP
            // 
            this.textBoxIP.Location = new System.Drawing.Point(44, 6);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(186, 20);
            this.textBoxIP.TabIndex = 2;
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(277, 6);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(58, 20);
            this.textBoxPort.TabIndex = 3;
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(341, 5);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(55, 23);
            this.buttonConnect.TabIndex = 4;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.Location = new System.Drawing.Point(402, 5);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(70, 23);
            this.buttonDisconnect.TabIndex = 17;
            this.buttonDisconnect.Text = "Disconnect";
            this.buttonDisconnect.UseVisualStyleBackColor = true;
            this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnect_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(118, 287);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Status :";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(167, 287);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(33, 13);
            this.StatusLabel.TabIndex = 19;
            this.StatusLabel.Text = "None";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.Location = new System.Drawing.Point(12, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 21;
            this.label5.Text = "Sensors :";
            // 
            // labelL1
            // 
            this.labelL1.AutoSize = true;
            this.labelL1.Location = new System.Drawing.Point(15, 70);
            this.labelL1.Name = "labelL1";
            this.labelL1.Size = new System.Drawing.Size(68, 13);
            this.labelL1.TabIndex = 30;
            this.labelL1.Text = "[L1] Corner 1";
            // 
            // labelL2
            // 
            this.labelL2.AutoSize = true;
            this.labelL2.Location = new System.Drawing.Point(15, 93);
            this.labelL2.Name = "labelL2";
            this.labelL2.Size = new System.Drawing.Size(68, 13);
            this.labelL2.TabIndex = 31;
            this.labelL2.Text = "[L2] Corner 2";
            // 
            // labelT
            // 
            this.labelT.AutoSize = true;
            this.labelT.Location = new System.Drawing.Point(15, 116);
            this.labelT.Name = "labelT";
            this.labelT.Size = new System.Drawing.Size(61, 13);
            this.labelT.TabIndex = 32;
            this.labelT.Text = "[T] Cable H";
            // 
            // labelS
            // 
            this.labelS.AutoSize = true;
            this.labelS.Location = new System.Drawing.Point(15, 139);
            this.labelS.Name = "labelS";
            this.labelS.Size = new System.Drawing.Size(41, 13);
            this.labelS.TabIndex = 33;
            this.labelS.Text = "[S] Slot";
            // 
            // labelCS
            // 
            this.labelCS.AutoSize = true;
            this.labelCS.Location = new System.Drawing.Point(15, 161);
            this.labelCS.Name = "labelCS";
            this.labelCS.Size = new System.Drawing.Size(96, 13);
            this.labelCS.TabIndex = 34;
            this.labelCS.Text = "[CS] Chariot Stable";
            // 
            // labelAP
            // 
            this.labelAP.AutoSize = true;
            this.labelAP.Location = new System.Drawing.Point(15, 183);
            this.labelAP.Name = "labelAP";
            this.labelAP.Size = new System.Drawing.Size(95, 13);
            this.labelAP.TabIndex = 35;
            this.labelAP.Text = "[AP] Position Arbre";
            // 
            // labelPP
            // 
            this.labelPP.AutoSize = true;
            this.labelPP.Location = new System.Drawing.Point(15, 206);
            this.labelPP.Name = "labelPP";
            this.labelPP.Size = new System.Drawing.Size(72, 13);
            this.labelPP.TabIndex = 36;
            this.labelPP.Text = "[PP] Plongeur";
            // 
            // labelDE
            // 
            this.labelDE.AutoSize = true;
            this.labelDE.Location = new System.Drawing.Point(15, 227);
            this.labelDE.Name = "labelDE";
            this.labelDE.Size = new System.Drawing.Size(78, 13);
            this.labelDE.TabIndex = 37;
            this.labelDE.Text = "[DE] Dispenser";
            // 
            // textBoxCode
            // 
            this.textBoxCode.AcceptsTab = true;
            this.textBoxCode.AllowDrop = true;
            this.textBoxCode.Location = new System.Drawing.Point(121, 37);
            this.textBoxCode.Multiline = true;
            this.textBoxCode.Name = "textBoxCode";
            this.textBoxCode.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxCode.Size = new System.Drawing.Size(351, 239);
            this.textBoxCode.TabIndex = 38;
            // 
            // buttonExecute
            // 
            this.buttonExecute.Location = new System.Drawing.Point(9, 282);
            this.buttonExecute.Name = "buttonExecute";
            this.buttonExecute.Size = new System.Drawing.Size(102, 23);
            this.buttonExecute.TabIndex = 39;
            this.buttonExecute.Text = "Execute";
            this.buttonExecute.UseVisualStyleBackColor = true;
            this.buttonExecute.Click += new System.EventHandler(this.buttonExecute_Click);
            // 
            // buttonCheck
            // 
            this.buttonCheck.Location = new System.Drawing.Point(8, 253);
            this.buttonCheck.Name = "buttonCheck";
            this.buttonCheck.Size = new System.Drawing.Size(102, 23);
            this.buttonCheck.TabIndex = 40;
            this.buttonCheck.Text = "Check";
            this.buttonCheck.UseVisualStyleBackColor = true;
            this.buttonCheck.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(274, 287);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 41;
            this.label4.Text = "Code Status :";
            // 
            // codeStatusLabel
            // 
            this.codeStatusLabel.AutoSize = true;
            this.codeStatusLabel.Location = new System.Drawing.Point(351, 287);
            this.codeStatusLabel.Name = "codeStatusLabel";
            this.codeStatusLabel.Size = new System.Drawing.Size(24, 13);
            this.codeStatusLabel.TabIndex = 42;
            this.codeStatusLabel.Text = "Idle";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Test 1 (simple - actu)",
            "Test 2 (simple - capt)",
            "Test 3 (boucle)",
            "Test 4",
            "Test 5 (sequence)"});
            this.comboBox1.Location = new System.Drawing.Point(277, 309);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 43;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // GUIPetra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 342);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.codeStatusLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonCheck);
            this.Controls.Add(this.buttonExecute);
            this.Controls.Add(this.textBoxCode);
            this.Controls.Add(this.labelDE);
            this.Controls.Add(this.labelPP);
            this.Controls.Add(this.labelAP);
            this.Controls.Add(this.labelCS);
            this.Controls.Add(this.labelS);
            this.Controls.Add(this.labelT);
            this.Controls.Add(this.labelL2);
            this.Controls.Add(this.labelL1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonDisconnect);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.textBoxIP);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GUIPetra";
            this.Text = "PetraClient";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelL1;
        private System.Windows.Forms.Label labelL2;
        private System.Windows.Forms.Label labelT;
        private System.Windows.Forms.Label labelS;
        private System.Windows.Forms.Label labelCS;
        private System.Windows.Forms.Label labelAP;
        private System.Windows.Forms.Label labelPP;
        private System.Windows.Forms.Label labelDE;
        private System.Windows.Forms.TextBox textBoxCode;
        private System.Windows.Forms.Button buttonExecute;
        private System.Windows.Forms.Button buttonCheck;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label codeStatusLabel;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}

