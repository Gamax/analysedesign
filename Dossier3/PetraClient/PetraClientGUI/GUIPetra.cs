﻿using PetraLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Petra_client
{
    public partial class GUIPetra : Form
    {
        private SocketClient socket;
        private bool isConnected = false;
        private PetraDataClass petraData;
        private Thread threadupdate;
        private bool runThread = false;
        private PetraSyntaxAnalyser syntaxAnalyser;

        public GUIPetra()
        {
            InitializeComponent();
            petraData = new PetraDataClass();
            setStatus("Disconnected");
            syntaxAnalyser = new PetraSyntaxAnalyser(petraData);

            threadupdate = new Thread(ThreadMethod);
            threadupdate.Start();

            buttonCheck.Enabled = false;
            buttonExecute.Enabled = false;


        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {

            if(isConnected == true)
            {
                MessageBox.Show("Already Connected to Petra", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            //Test de la présence de données dans les 2 champs
            if(textBoxIP.Text == "" || textBoxPort.Text == "")
            {
                MessageBox.Show("Please enter IP and Port", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            socket = new SocketClient();
            try
            {
                socket.Connect(textBoxIP.Text, Int32.Parse(textBoxPort.Text));
            }
            catch(SocketException error)
            {
                MessageBox.Show("Cannot connect to Petra : " + error.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            runThread = true;
            isConnected = true;
            setStatus("Connected");
            buttonCheck.Enabled = true;
            buttonExecute.Enabled = true;

        }

        public void setStatus(String text)
        {
            StatusLabel.Text = text;
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            if(isConnected == false)
            {
                return;
            }

            //threadupdate.Abort();
            runThread = false;
            socket.Close();
            socket = null;
            isConnected = false;
            setStatus("Disconnected");
            buttonCheck.Enabled = false;
            buttonExecute.Enabled = false;
        }

        private void updatecapteur()
        {
            if(petraData.captL1 == 1)
            {
                labelL1.BackColor = Color.Green;
            }
            else
            {
                labelL1.BackColor = default(Color);
            }

            if (petraData.captL2 == 1)
            {
                labelL2.BackColor = Color.Green;
            }
            else
            {
                labelL2.BackColor = default(Color);
            }

            if (petraData.captT == 1)
            {
                labelT.BackColor = Color.Green;
            }
            else
            {
                labelT.BackColor = default(Color);
            }

            if (petraData.captS == 1)
            {
                labelS.BackColor = Color.Green;
            }
            else
            {
                labelS.BackColor = default(Color);
            }

            if (petraData.captCS == 1)
            {
                labelCS.BackColor = Color.Green;
            }
            else
            {
                labelCS.BackColor = default(Color);
            }

            if (petraData.captAP == 1)
            {
                labelAP.BackColor = Color.Green;
            }
            else
            {
                labelAP.BackColor = default(Color);
            }

            if (petraData.captPP == 1)
            {
                labelPP.BackColor = Color.Green;
            }
            else
            {
                labelPP.BackColor = default(Color);
            }

            if (petraData.captDE == 1)
            {
                labelDE.BackColor = Color.Green;
            }
            else
            {
                labelDE.BackColor = default(Color);
            }
        }

        private void ThreadMethod()
        {
            while (true)
            {

                if (runThread)
                {
                    while (runThread)
                    {
                        try
                        {
                            socket.Send(petraData.GetActuatorMessage());
                            petraData.SetCaptorFromMessage(socket.Receive());
                        }
                        catch(SocketException error)
                        {
                            isConnected = false;
                            socket.Close();
                            return;
                        }

                        updatecapteur();

                        Thread.Sleep(100); //todo change
                    }
                }
                else
                    Thread.Sleep(2000);
            }
        }

        private void buttonExecute_Click(object sender, EventArgs e)
        {

            try
            {

                codeStatusLabel.Text = "Executing";
                buttonExecute.Enabled = false;
                buttonCheck.Enabled = false;
                syntaxAnalyser.Execute(textBoxCode.Text);
              


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur : " + ex.Message,"ERREUR", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            buttonExecute.Enabled = true;
            buttonCheck.Enabled = true;
            codeStatusLabel.Text = "Idle";
        }

        private void buttonCheck_Click(object sender, EventArgs e)
        {
            try
            {
                syntaxAnalyser.Check(textBoxCode.Text);
                MessageBox.Show("Code is valid, ready for Petra", "Well done", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur : " + ex.Message, "ERREUR", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((String)comboBox1.SelectedItem == "Test 1 (simple - actu)")
            {
                textBoxCode.Text = @"SET C1 1
WAIT 2
SET C1 0
WAIT 2
SET C2 1
WAIT 2
SET C2 0";
            }

            if ((String)comboBox1.SelectedItem == "Test 2 (simple - capt)")
            {
                textBoxCode.Text = @"IF DE 1
	SET PA 1
	WAIT 2
	SET PV 1
	WAIT 2
	SET PA 0
	WAIT 2
	SET PA 1
	WAIT 2
	SET PV 0
ENDIF
";
            }

            if ((String)comboBox1.SelectedItem == "Test 3 (boucle)")
            {
                textBoxCode.Text = @"SET C1 1
LOOP S 0
ENDLOOP
WAIT 1
SET C1 0";
            }

            if ((String)comboBox1.SelectedItem == "Test 4")
            {
                textBoxCode.Text = @"";
            }

            if ((String)comboBox1.SelectedItem == "Test 5 (sequence)")
            {
                textBoxCode.Text = @"IF DE 1
	SET PA 1
	WAIT 2
	SET PV 1
	WAIT 2
	SET PA 0
	WAIT 2
	SET CP 1
	WAIT 2
	SET PV 0
	WAIT 2
	SET C1 1
	SET CP 0
	LOOP S 0
	ENDLOOP
	WAIT 1
	SET C1 0
ENDIF";
            }
        }
    }
}
