﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetraLibrary
{
    public class PetraDataClass
    {
        public int captL1 = 0;
        public int captL2 = 0;
        public int captT = 0;
        public int captS = 0;
        public int captCS = 0;
        public int captAP = 0;
        public int captPP = 0;
        public int captDE = 0;

        public int actCP = 0; //0 à 3
        public int actC1 = 0;
        public int actC2 = 0;
        public int actPV = 0;
        public int actPA = 0;
        public int actAA = 0;
        public int actGA = 0;

        public String GetActuatorMessage()
        {
            return actCP.ToString() + actC1.ToString() + actC2.ToString() + actPV.ToString() + actPA.ToString() + actAA.ToString() + actGA.ToString() + '0';
        }

        public void SetCaptorFromMessage(String message)
        {
            captL1 = (int) Char.GetNumericValue(message[0]);
            captL2 = (int)Char.GetNumericValue(message[1]);
            captT = (int)Char.GetNumericValue(message[2]);
            captS = (int)Char.GetNumericValue(message[3]);
            captCS = (int)Char.GetNumericValue(message[4]);
            captAP = (int)Char.GetNumericValue(message[5]);
            captPP = (int)Char.GetNumericValue(message[6]);
            captDE = (int)Char.GetNumericValue(message[7]);
        }

        public void SetActuator(actuatorEnum actuator,int value)
        {
            switch (actuator)
            {
                case actuatorEnum.Null:
                    break;
                case actuatorEnum.CP:
                    actCP = value;
                    break;
                case actuatorEnum.C1:
                    actC1 = value;
                    break;
                case actuatorEnum.C2:
                    actC2 = value;
                    break;
                case actuatorEnum.PV:
                    actPV = value;
                    break;
                case actuatorEnum.PA:
                    actPA = value;
                    break;
                case actuatorEnum.AA:
                    actAA = value;
                    break;
                case actuatorEnum.GA:
                    actGA = value;
                    break;
            }
        }
        public int GetCaptor(captorEnum captor)
        {
            switch (captor)
            {
                case captorEnum.L1:
                    return captL1;
                case captorEnum.L2:
                    return captL2;
                case captorEnum.T:
                    return captT;
                case captorEnum.S:
                    return captS;
                case captorEnum.CS:
                    return captCS;
                case captorEnum.AP:
                    return captAP;
                case captorEnum.PP:
                    return captPP;
                case captorEnum.DE:
                    return captDE;
            }
            return -1;
        }

        public int GetActuator(actuatorEnum actuator)
        {
            switch (actuator)
            {
                case actuatorEnum.CP:
                    return actCP;
                case actuatorEnum.C1:
                    return actC1;
                case actuatorEnum.C2:
                    return actC2;
                case actuatorEnum.PV:
                    return actPV;
                case actuatorEnum.PA:
                    return actPA;
                case actuatorEnum.AA:
                   return actAA;
                case actuatorEnum.GA:
                    return actGA;
            }
            return -1;
        }
    }
}