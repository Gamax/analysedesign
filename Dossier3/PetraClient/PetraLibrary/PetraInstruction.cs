﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetraLibrary
{
    public enum commandTypeEnum { IF, ENDIF, WAIT, SET, LOOP, ENDLOOP};
    public enum captorEnum {Null,L1,L2,T,S,CS,AP,PP,DE};
    public enum actuatorEnum {Null,CP,C1,C2,PV,PA,AA,GA};
   
    public class PetraInstruction
    {
        public commandTypeEnum commandType;
        public int numberParameter = -1;
        public captorEnum captor;
        public actuatorEnum actuator;

        public PetraInstruction(String line)
        {
            String tempString = (line.Split(' '))[0];

            if (!Enum.TryParse<commandTypeEnum>(tempString, out commandType))
            {
                throw new InvalidPetraInstructionException("["+tempString+ "] isn't a valid identifier");
            }

            //si endif fin du traitement
            if (commandType == commandTypeEnum.ENDIF)
                return;

            if (commandType == commandTypeEnum.ENDLOOP)
                return;

            tempString = (line.Split(' '))[1];

            //si wait
            if (commandType == commandTypeEnum.WAIT)
            {
                if(!(int.TryParse(tempString, out numberParameter) && numberParameter > 0))
                {
                    //parse de l'arg raté
                    throw new InvalidPetraInstructionException("[" + tempString + "] isn't a valid parameter");
                }
                return;
            }

            //si set
            if (commandType == commandTypeEnum.SET && !Enum.TryParse<actuatorEnum>(tempString, out actuator))
            {
                throw new InvalidPetraInstructionException("[" + tempString + "] isn't a valid actuator");
            }

            
            //il reste le if
            if (commandType == commandTypeEnum.IF && !Enum.TryParse<captorEnum>(tempString, out captor) && !Enum.TryParse<actuatorEnum>(tempString, out actuator))
            {
                throw new InvalidPetraInstructionException("[" + tempString + "] isn't a valid captor/actuator");
            }

            //loop
            if (commandType == commandTypeEnum.LOOP && !Enum.TryParse<captorEnum>(tempString, out captor) && !Enum.TryParse<actuatorEnum>(tempString, out actuator))
            {
                throw new InvalidPetraInstructionException("[" + tempString + "] isn't a valid captor/actuator");
            }

            //if et set prenne la valeur en 3ème argument

            tempString = (line.Split(' '))[2];

            if(!int.TryParse(tempString, out numberParameter))
            {
                //pas un nombre
                throw new InvalidPetraInstructionException("[" + tempString + "] isn't a valid 3rd argument for if and set");
            }

            if (actuator == actuatorEnum.CP && numberParameter > -1 && numberParameter < 4)
                return; //number parameter peut avori ces valeurs uniquement si CP

            if(numberParameter != 0 && numberParameter != 1)
            {
                throw new InvalidPetraInstructionException("[" + tempString + "] isn't a valid value for if and set");
            }
             //tout est bon sinon
        }
    }
}
