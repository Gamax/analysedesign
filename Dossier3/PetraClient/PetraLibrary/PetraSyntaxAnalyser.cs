﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PetraLibrary
{
    public class PetraSyntaxAnalyser
    {
        private PetraDataClass petraData;
        private List<String> linesList = new List<string>();
        private List<PetraInstruction> instructionsList = new List<PetraInstruction>();
        public PetraSyntaxAnalyser(PetraDataClass petraData)
        {
            this.petraData = petraData;
        }

        private void SplitLines(String s)
        {
            //analyse string by string
            foreach (var lineString in s.Split(new string[] { Environment.NewLine,"\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                if(!string.IsNullOrWhiteSpace(lineString))
                {
                    linesList.Add(lineString.TrimStart());
                    Console.WriteLine(lineString.TrimStart());
                }
                    
            }
        }

        private void ParseCode()
        {
            foreach(String line in linesList)
            {
                //Vérification de la syntaxe + ajout dans une liste
                instructionsList.Add(new PetraInstruction(line));
            }

        }

        private void ExecuteCode()
        {
            int posLoop;

            for(int i=0;i<instructionsList.Count;i++)
            {

                switch (instructionsList[i].commandType)
                {
                    case commandTypeEnum.WAIT:
                        Thread.Sleep(instructionsList[i].numberParameter * 1000);
                        break;
                    case commandTypeEnum.SET:
                        petraData.SetActuator(instructionsList[i].actuator, instructionsList[i].numberParameter);
                        break;
                    case commandTypeEnum.IF:
                        //check condition

                        if (instructionsList[i].numberParameter != petraData.GetCaptor(instructionsList[i].captor) &&
                            instructionsList[i].numberParameter != petraData.GetActuator(instructionsList[i].actuator))
                        {
                            //cond pas respectée donc jump à prochain end if
                            i = findNextCommand(i,commandTypeEnum.ENDIF)-1;
                        }

                        break;
                    case commandTypeEnum.LOOP:
                        //check condition

                        if (instructionsList[i].numberParameter != petraData.GetCaptor(instructionsList[i].captor) &&
                            instructionsList[i].numberParameter != petraData.GetActuator(instructionsList[i].actuator))
                        {
                            //cond pas respectée donc jump à prochain end loop
                            i = findNextCommand(i, commandTypeEnum.ENDLOOP)-1;
                        }

                        break;
                    case commandTypeEnum.ENDIF:
                        break;
                    case commandTypeEnum.ENDLOOP:
                        posLoop = findPreviousCommand(i, commandTypeEnum.LOOP); //pos entrée loop

                        if (instructionsList[posLoop].numberParameter == petraData.GetCaptor(instructionsList[posLoop].captor) ||
                            instructionsList[posLoop].numberParameter == petraData.GetActuator(instructionsList[posLoop].actuator))
                            i = posLoop-1; //si cond vérifiée on jump vers l'entrée de la boucle
                        break;
                }
            }
        }

        private int findNextCommand(int currentElement,commandTypeEnum command)
        {
            int i;
            for (i = currentElement; i < instructionsList.Count - 1 && instructionsList[i].commandType != command; i++);
            return i;
        }

        private int findPreviousCommand(int currentElement, commandTypeEnum command)
        {
            int i;
            for (i = currentElement; i > 0 && instructionsList[i].commandType != command; i--) ;
            return i;
        }

        public void Check(String s)
        {
            Reset();
            SplitLines(s);
            ParseCode();
        }

        public void Execute(String s)
        {
            Check(s);
            ExecuteCode();
        }

        private void Reset()
        {
            linesList.Clear();
            instructionsList.Clear();
        }
    }
}
