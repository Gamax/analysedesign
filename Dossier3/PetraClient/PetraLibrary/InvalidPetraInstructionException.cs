﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetraLibrary
{
    public class InvalidPetraInstructionException : Exception
    {
        public InvalidPetraInstructionException()
        {
        }
        public InvalidPetraInstructionException(String message) : base(message)
        {
        }
    }
}
