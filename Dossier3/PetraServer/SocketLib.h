#ifndef SOCKETLIB_H
#define SOCKETLIB_H

int socketlib_create();
int socketlib_send(int socket_descriptor,char* message);
int socketlib_receive(int socket_descriptor, char* message);

/**
 *
 * return : -2 if get hostname failed
 *          -1 if bind failed
 *          0 if everything went acording to the plan
 */

int socketlib_bind(int socket_descriptor, char *address, unsigned short port);
int socketlib_listen(int socket_descriptor);
int socketlib_accept(int socket_descriptor);
int socketlib_receive_unfixed_size(int hSocket, char *msgclient);
#endif
