#include <stdlib.h>
#include <stdio.h>
#include "SocketLib.h"
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <time.h>

#define DEBUG 0 //todo ATTENTION A CA !

struct	ACTUATEURS
{
    unsigned CP : 2;
    unsigned C1 : 1;
    unsigned C2 : 1;
    unsigned PV : 1;
    unsigned PA : 1;
    unsigned AA : 1;
    unsigned GA : 1;
};

/*
 * 0 -> [CP] Chariot 2 bytes
 * 2 -> [C1] Conv 1
 * 3 -> [C2] Conv 2
 * 4 -> [PV] Ventouse
 * 5 -> [PA] Plongeur
 * 6 -> [AA] Arbre
 * 7 -> [GA] Grapin arbre
 */

union
{
    struct ACTUATEURS act ;
    unsigned char byte ;
} u_act;

struct 	CAPTEURS
{
    unsigned L1 : 1;
    unsigned L2 : 1;
    unsigned T  : 1;
    unsigned S  : 1;
    unsigned CS : 1;
    unsigned AP : 1;
    unsigned PP : 1;
    unsigned DE : 1;
};

/*
 * 0 -> [L1] corner 1
 * 1 -> [L2] corner 2
 * 2 -> [T] cabl� H ?????
 * 3 -> [S] slot
 * 4 -> [CS] chariot stable
 * 5 -> [AP] capteur position arbre
 * 6 -> [PP] capteur plogneur
 * 7 -> [DE] capteur dispenser
 */

union
{
    struct CAPTEURS capt ;
    unsigned char byte ;
} u_capt;

int fd_petra_in,fd_petra_out;

int connectionSocket,listenSocket;
int request_processing(char * message);
int set_actuator(unsigned short value,int actu_num);
int read_sensor(char * message);

//args : "test"/"server", address, port


int main(int argc, char *argv[]){
	
	if(argc != 4)
	{
		printf("Wrong number of arguments\n");
		return EXIT_FAILURE;
	}

	if(strcmp(argv[1],"test")==0)
	{
		printf("Entering test mode\n");
		    char message[100];

			printf("Creating Socket\n");
		listenSocket = socketlib_create();

			printf("Binding Server\n");
        socketlib_bind(listenSocket, argv[2], atoi(argv[3]));

			printf("Listening Server\n");
			socketlib_listen(listenSocket);

			printf("Accepting Server\n");
			int socketclient = socketlib_accept(listenSocket);

			printf("Receiving Server\n");
			//int bytesrec = socketlib_receive(socketclient,message);
			int bytesrec = socketlib_receive_unfixed_size(socketclient, message); // --> Fonctionne !!
			printf("%d bytes recu - errno : %s\n",bytesrec,strerror(errno));
			printf("Received Message from client : [%s]\n",message);

			printf("Sending server\n");
			strcpy(message,"Hello c'est le serveur");
			int bytessend = socketlib_send(socketclient,message);
			printf("%d bytes envoy�\n",bytessend);
			return EXIT_SUCCESS;
	}

	char message[100];

	printf("Entering server mode...\n");

	clock_t starttime, currenttime;
	starttime = clock();

	if(!DEBUG){

    printf("Initializing Petra interface...\n");
    fd_petra_out = open( "/dev/actuateursPETRA",O_WRONLY);
    if (fd_petra_out == -1)
    {
        perror ("MAIN : Erreur ouverture PETRA_OUT");
        return 1;
    }
    else
        printf ("MAIN: PETRA_OUT opened\n");

    fd_petra_in = open( "/dev/capteursPETRA",O_RDONLY);
    if (fd_petra_in == -1)
    {
        perror ("MAIN : Erreur ouverture PETRA_IN");
        return 1;
    }
    else
        printf ("MAIN: PETRA_IN opened\n");
	}else{
		printf("Launching in debug mode, Petra not loaded...\n");
	}

    //Mise � 0 du byte des actuateurs
    u_act.byte = 0x00 ;

    if(DEBUG)
    {
		u_capt.capt.L1 = 1; //todo remove
		u_capt.capt.AP = 0;
		u_capt.capt.L2 = 0;
		u_capt.capt.CS = 1;
		u_capt.capt.DE = 0;
		u_capt.capt.PP = 1;
		u_capt.capt.S = 0;
		u_capt.capt.T = 1;
    }

	printf("Creating socket\n");
	int socketlisten = socketlib_create();

	printf("Binding Server on %s:%s\n",argv[2],argv[3]);
    socketlib_bind(socketlisten, argv[2], atoi(argv[3]));


	while(1)
	{
		socketlib_listen(socketlisten);

		printf("Waiting for connection...\n");

		connectionSocket = socketlib_accept(socketlisten);

		printf("Connection established !\n");

		while(1)
		{



			if(socketlib_receive_unfixed_size(connectionSocket,message) < 0){
				printf("Client disconnected\n");
				close(connectionSocket);
				break;
			}
			currenttime = clock();

            printf("[%6.3f] Received message : [%s]\n",(double) ((currenttime-starttime)/ CLOCKS_PER_SEC),message);

			request_processing(message);

			currenttime = clock();

			printf("[%6.3f] Message sent : [%s]\n",(double) ((currenttime-starttime)/ CLOCKS_PER_SEC),message);

			socketlib_send(connectionSocket,message);

			fflush(stdout);

		}

	}

}

int request_processing(char * message){
    if(strlen(message)!=8){
        printf("E: Wrong message size !\n");
        return -1;
    }

    /******************************************
     * PHASE 1 : Ecriture Actuator
     ******************************************/

    int i;
    int return_value;

    switch (message[0]){
        case '0':
            u_act.act.CP = 0;
            break;
        case '1':
            u_act.act.CP = 1;
            break;
        case '2':
            u_act.act.CP = 2;
            break;
        case '3':
            u_act.act.CP = 3;
            break;
        default:
            printf("E: Wrong character in message !\n");
            return -1;
    }

    for(i=1;i<7;i++) //on regarde les 6 premiers bits, le chariot est trait� � part
    {
        switch (message[i])
        {
            case '1':
                return_value =set_actuator(1,i);
                break;
            case '0':
                return_value =set_actuator(0,i);
                break;
            default:
                printf("E: Wrong character in message !\n");
                return -1;
        }

        if(return_value < 0)
        {
            printf("E: set_actuator wrong index !\n");
            return -1;
        }

    }

    //�criture
    if(!DEBUG)
    	write(fd_petra_out,&u_act.byte,1);

    /******************************************
     * PHASE 2 : Lecture Sensor
     ******************************************/
    if(!DEBUG)
    	read(fd_petra_in,&u_capt.byte,1);

    /*
    u_capt.capt.L1 ^= 1;
        u_capt.capt.AP ^= 1;
        u_capt.capt.L2 ^= 1;
        u_capt.capt.CS ^= 1;
        u_capt.capt.DE ^= 1;
        u_capt.capt.PP ^= 1;
        u_capt.capt.S ^= 1;
        u_capt.capt.T ^= 1;
        */

    read_sensor(message);

    return 0;

}

int set_actuator(unsigned short value,int actu_num){
    switch (actu_num)
    {
        case 1:
            u_act.act.C1 = value;
            break;
        case 2:
            u_act.act.C2 = value;
            break;
        case 3:
            u_act.act.PV = value;
            break;
        case 4:
            u_act.act.PA = value;
            break;
        case 5:
            u_act.act.AA = value;
            break;
        case 6:
            u_act.act.GA = value;
            break;
        default:
            return -1;
    }

    return 0;
}

int read_sensor(char * message){

    if(u_capt.capt.L1 == 0)
        message[0] = '0';
    else
        message[0] = '1';

    if(u_capt.capt.L2 == 0)
        message[1] = '0';
    else
        message[1] = '1';

    if(u_capt.capt.T == 0)
        message[2] = '0';
    else
        message[2] = '1';

    if(u_capt.capt.S == 0)
        message[3] = '0';
    else
        message[3] = '1';

    if(u_capt.capt.CS == 0)
        message[4] = '0';
    else
        message[4] = '1';

    if(u_capt.capt.AP == 0)
        message[5] = '0';
    else
        message[5] = '1';

    if(u_capt.capt.PP == 0)
        message[6] = '0';
    else
        message[6] = '1';

    if(u_capt.capt.DE == 0)
        message[7] = '0';
    else
        message[7] = '1';

    return 0;
}
