# Dossier 2

## Détails implémentation

### Message
#### Client vers Server (Petra)

7 caractères :

- 1er : soit 0/1/2/3 -> position chariot

- le reste : soit 0/1 -> état autres actuateurs

```c
  /*
   * 0 -> [CP] Chariot 2 bytes
   * 2 -> [C1] Conv 1
   * 3 -> [C2] Conv 2
   * 4 -> [PV] Ventouse
   * 5 -> [PA] Plongeur
   * 6 -> [AA] Arbre
   * 7 -> [GA] Grapin arbre
   */
```

#### Server  (Petra) vers Client

8 caractères :
- état des capteurs : soit 0/1

```c
/*
 * 0 -> [L1] corner 1
 * 1 -> [L2] corner 2
 * 2 -> [T] cablé H ?????
 * 3 -> [S] slot
 * 4 -> [CS] chariot stable
 * 5 -> [AP] capteur position arbre
 * 6 -> [PP] capteur plogneur
 * 7 -> [DE] capteur dispenser
 */
```



# Dossier 3

## Language

### Instructions

#### IF

IF *nomcapteur/acti*

*expr* 

ENDIF

#### WAIT

WAIT *nbmilliseconde*

#### SET

SET *nomactu* *état*

## TestCode



```Petr
SET CP 1
WAIT 2
SET C1 1
WAIT 2
SET C2 1
WAIT 2
SET CP 1
SET C1 0
WAIT 2
SET C2 0
WAIT 2
SET CP 0
```