#include "SocketLib.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
//Pour le receive
#include <netinet/in.h>
#include <netinet/tcp.h>

#define MAXSTRING 1024 //todo adapt

int socketlib_create(){
    return socket(AF_INET,SOCK_STREAM,0);
}
int socketlib_send(int socket_descriptor,char* message){
    return send(socket_descriptor,message,strlen(message),0); //Strlen ! ;)
}
int socketlib_receive(int socket_descriptor, char* message){
    return recv(socket_descriptor,message,MAXSTRING,0);
}

int socketlib_bind(int socket_descriptor, char *address, unsigned short port) {
    //struct hostent * infosHost;
    struct sockaddr_in socketAdress;
/*
    char hostname[1024];
    gethostname(hostname, 1023);
    hostname[1023] = '\0';
    if((infosHost = gethostbyname(hostname))==0)
        return -2;
*/
    //preparation sock_addr_in
    memset(&socketAdress,0, sizeof(struct sockaddr_in));
    socketAdress.sin_family = AF_INET;
    socketAdress.sin_port = htons(port); //htons converti le nombre en format réseau
    socketAdress.sin_addr.s_addr = inet_addr(address);

    if(bind(socket_descriptor,(struct sockaddr *)&socketAdress, sizeof(struct sockaddr_in))==-1) //association de socketAdress avec socket
        return -1;

    return 0;
}
int socketlib_listen(int socket_descriptor){
    return listen(socket_descriptor,SOMAXCONN);
}
int socketlib_accept(int socket_descriptor){
    struct sockaddr_in socketAdressClient;
    socklen_t Sockaddr_in_size = sizeof(struct sockaddr_in);
    //ConnectionSocket est le socket connecté au client, le socket principal reste en listening
    return accept(socket_descriptor,(struct sockaddr*)&socketAdressClient,&Sockaddr_in_size);
}
// Fonctions n�cessaires au receive
static int socketlib_get_MTU(int hSocket)
{
    int tailleO,tailleS;

    tailleO=sizeof(int);
    if (getsockopt(hSocket, IPPROTO_TCP, TCP_MAXSEG,&tailleS,(socklen_t*) &tailleO) == -1)
    {
        //printf("Erreur sur le getsockopt de la socket %d\n", errno);
       // exit(1);
    	return -1;
    }

    return tailleS;
}
static char socketlib_marqueurRecu(char *m, int nc)
/* Recherche de la sequence \r\n */
{
    static char demiTrouve=0;
    int i;
    char trouve=0;

    if (demiTrouve==1 && m[0]=='\n') return 1;
    else demiTrouve=0;
    for (i=0; i<nc-1 && !trouve; i++)
        if (m[i]=='\r' && m[i+1]=='\n') trouve=1;
    if (trouve) return 1;
    else if (m[nc]=='\r')
    {
        demiTrouve=1;
        return 0;
    }
    else return 0;
}
int socketlib_receive_unfixed_size(int hSocket, char *msgclient)
{
    int tailleMsgRecu = 0;
    int finDetectee = 0;
    int nbreBytesRecus;
    int MTU = socketlib_get_MTU(hSocket);
    if(MTU < 0)
    	return -1;
    char buf[MTU];

    memset(buf,0,sizeof(buf));
    do
    {
        if ((nbreBytesRecus = recv(hSocket, buf, MTU, 0)) == -1)
        {
            printf("Erreur sur le recv de la socket %d\n", errno);
            return -1;
            //close(hSocket);
            //exit(1);
        }
        else
        {
            finDetectee = socketlib_marqueurRecu(buf, nbreBytesRecus);
            memcpy((char *)msgclient + tailleMsgRecu, buf,nbreBytesRecus);
            tailleMsgRecu += nbreBytesRecus;
            /*printf("finDetecteee = %d\n", finDetectee);
            printf("Nombre de bytes recus = %d\n", nbreBytesRecus );
            printf("Taile totale msg recu = %d\n", tailleMsgRecu );*/
        }
    }
    while (nbreBytesRecus != 0 && nbreBytesRecus != -1 && !finDetectee);

    msgclient[tailleMsgRecu-2]=0; /* zero de fin de chaine */

     //  printf("Recv socket OK\n");

    return tailleMsgRecu;
}
