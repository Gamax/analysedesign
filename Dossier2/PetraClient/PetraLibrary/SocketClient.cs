﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PetraLibrary
{
    public class SocketClient
    {
        private Socket socket;

        public SocketClient()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Connect(String StrIPAddress,int Port)
        {
            IPAddress address = IPAddress.Parse(StrIPAddress);
            IPEndPoint remoteEP = new IPEndPoint(address, Port);
            socket.Connect(remoteEP);
        }

        public int Send(String Message)
        {
            byte[] msg = Encoding.ASCII.GetBytes(Message + "\r\n");
            return socket.Send(msg);
        }

        public String Receive()
        {
            byte[] buffer = new byte[1024];
            int nbBytesRec = socket.Receive(buffer);
            Console.WriteLine(nbBytesRec + " bytes reçu");
            return Encoding.ASCII.GetString(buffer, 0, nbBytesRec);

        }

        public void Close()
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }

    }
}
