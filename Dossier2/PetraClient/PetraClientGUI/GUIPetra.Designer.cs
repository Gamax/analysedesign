﻿namespace Petra_client
{
    partial class GUIPetra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxC1 = new System.Windows.Forms.CheckBox();
            this.checkBoxC2 = new System.Windows.Forms.CheckBox();
            this.checkBoxPV = new System.Windows.Forms.CheckBox();
            this.checkBoxPA = new System.Windows.Forms.CheckBox();
            this.checkBoxAA = new System.Windows.Forms.CheckBox();
            this.checkBoxGA = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.labelL1 = new System.Windows.Forms.Label();
            this.labelL2 = new System.Windows.Forms.Label();
            this.labelT = new System.Windows.Forms.Label();
            this.labelS = new System.Windows.Forms.Label();
            this.labelCS = new System.Windows.Forms.Label();
            this.labelAP = new System.Windows.Forms.Label();
            this.labelPP = new System.Windows.Forms.Label();
            this.labelDE = new System.Windows.Forms.Label();
            this.comboBoxCP = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Port : ";
            // 
            // textBoxIP
            // 
            this.textBoxIP.Location = new System.Drawing.Point(44, 6);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(110, 20);
            this.textBoxIP.TabIndex = 2;
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(201, 6);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(58, 20);
            this.textBoxPort.TabIndex = 3;
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(130, 33);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(55, 23);
            this.buttonConnect.TabIndex = 4;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.Location = new System.Drawing.Point(189, 33);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(70, 23);
            this.buttonDisconnect.TabIndex = 17;
            this.buttonDisconnect.Text = "Disconnect";
            this.buttonDisconnect.UseVisualStyleBackColor = true;
            this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnect_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Status :";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(51, 38);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(33, 13);
            this.StatusLabel.TabIndex = 19;
            this.StatusLabel.Text = "None";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(15, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "Actuators :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.Location = new System.Drawing.Point(141, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 21;
            this.label5.Text = "Sensors :";
            // 
            // checkBoxC1
            // 
            this.checkBoxC1.AutoSize = true;
            this.checkBoxC1.Location = new System.Drawing.Point(15, 100);
            this.checkBoxC1.Name = "checkBoxC1";
            this.checkBoxC1.Size = new System.Drawing.Size(97, 17);
            this.checkBoxC1.TabIndex = 22;
            this.checkBoxC1.Text = "     [C1] Conv 1";
            this.checkBoxC1.UseVisualStyleBackColor = true;
            this.checkBoxC1.CheckedChanged += new System.EventHandler(this.checkBoxC1_CheckedChanged);
            // 
            // checkBoxC2
            // 
            this.checkBoxC2.AutoSize = true;
            this.checkBoxC2.Location = new System.Drawing.Point(15, 123);
            this.checkBoxC2.Name = "checkBoxC2";
            this.checkBoxC2.Size = new System.Drawing.Size(97, 17);
            this.checkBoxC2.TabIndex = 23;
            this.checkBoxC2.Text = "     [C2] Conv 2";
            this.checkBoxC2.UseVisualStyleBackColor = true;
            this.checkBoxC2.CheckedChanged += new System.EventHandler(this.checkBoxC2_CheckedChanged);
            // 
            // checkBoxPV
            // 
            this.checkBoxPV.AutoSize = true;
            this.checkBoxPV.Location = new System.Drawing.Point(15, 146);
            this.checkBoxPV.Name = "checkBoxPV";
            this.checkBoxPV.Size = new System.Drawing.Size(109, 17);
            this.checkBoxPV.TabIndex = 24;
            this.checkBoxPV.Text = "     [PV] Ventouse";
            this.checkBoxPV.UseVisualStyleBackColor = true;
            this.checkBoxPV.CheckedChanged += new System.EventHandler(this.checkBoxPV_CheckedChanged);
            // 
            // checkBoxPA
            // 
            this.checkBoxPA.AutoSize = true;
            this.checkBoxPA.Location = new System.Drawing.Point(15, 169);
            this.checkBoxPA.Name = "checkBoxPA";
            this.checkBoxPA.Size = new System.Drawing.Size(106, 17);
            this.checkBoxPA.TabIndex = 25;
            this.checkBoxPA.Text = "     [PA] Plongeur";
            this.checkBoxPA.UseVisualStyleBackColor = true;
            this.checkBoxPA.CheckedChanged += new System.EventHandler(this.checkBoxPA_CheckedChanged);
            // 
            // checkBoxAA
            // 
            this.checkBoxAA.AutoSize = true;
            this.checkBoxAA.Location = new System.Drawing.Point(15, 192);
            this.checkBoxAA.Name = "checkBoxAA";
            this.checkBoxAA.Size = new System.Drawing.Size(89, 17);
            this.checkBoxAA.TabIndex = 26;
            this.checkBoxAA.Text = "     [AA] Arbre";
            this.checkBoxAA.UseVisualStyleBackColor = true;
            this.checkBoxAA.CheckedChanged += new System.EventHandler(this.checkBoxAA_CheckedChanged);
            // 
            // checkBoxGA
            // 
            this.checkBoxGA.AutoSize = true;
            this.checkBoxGA.Location = new System.Drawing.Point(15, 213);
            this.checkBoxGA.Name = "checkBoxGA";
            this.checkBoxGA.Size = new System.Drawing.Size(123, 17);
            this.checkBoxGA.TabIndex = 27;
            this.checkBoxGA.Text = "     [GA] Grapin arbre";
            this.checkBoxGA.UseVisualStyleBackColor = true;
            this.checkBoxGA.CheckedChanged += new System.EventHandler(this.checkBoxGA_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 237);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "[CP] Chariot";
            // 
            // labelL1
            // 
            this.labelL1.AutoSize = true;
            this.labelL1.Location = new System.Drawing.Point(144, 101);
            this.labelL1.Name = "labelL1";
            this.labelL1.Size = new System.Drawing.Size(68, 13);
            this.labelL1.TabIndex = 30;
            this.labelL1.Text = "[L1] Corner 1";
            // 
            // labelL2
            // 
            this.labelL2.AutoSize = true;
            this.labelL2.Location = new System.Drawing.Point(144, 124);
            this.labelL2.Name = "labelL2";
            this.labelL2.Size = new System.Drawing.Size(68, 13);
            this.labelL2.TabIndex = 31;
            this.labelL2.Text = "[L2] Corner 2";
            // 
            // labelT
            // 
            this.labelT.AutoSize = true;
            this.labelT.Location = new System.Drawing.Point(144, 147);
            this.labelT.Name = "labelT";
            this.labelT.Size = new System.Drawing.Size(61, 13);
            this.labelT.TabIndex = 32;
            this.labelT.Text = "[T] Cable H";
            // 
            // labelS
            // 
            this.labelS.AutoSize = true;
            this.labelS.Location = new System.Drawing.Point(144, 170);
            this.labelS.Name = "labelS";
            this.labelS.Size = new System.Drawing.Size(41, 13);
            this.labelS.TabIndex = 33;
            this.labelS.Text = "[S] Slot";
            // 
            // labelCS
            // 
            this.labelCS.AutoSize = true;
            this.labelCS.Location = new System.Drawing.Point(144, 192);
            this.labelCS.Name = "labelCS";
            this.labelCS.Size = new System.Drawing.Size(96, 13);
            this.labelCS.TabIndex = 34;
            this.labelCS.Text = "[CS] Chariot Stable";
            // 
            // labelAP
            // 
            this.labelAP.AutoSize = true;
            this.labelAP.Location = new System.Drawing.Point(144, 214);
            this.labelAP.Name = "labelAP";
            this.labelAP.Size = new System.Drawing.Size(95, 13);
            this.labelAP.TabIndex = 35;
            this.labelAP.Text = "[AP] Position Arbre";
            // 
            // labelPP
            // 
            this.labelPP.AutoSize = true;
            this.labelPP.Location = new System.Drawing.Point(144, 237);
            this.labelPP.Name = "labelPP";
            this.labelPP.Size = new System.Drawing.Size(72, 13);
            this.labelPP.TabIndex = 36;
            this.labelPP.Text = "[PP] Plongeur";
            // 
            // labelDE
            // 
            this.labelDE.AutoSize = true;
            this.labelDE.Location = new System.Drawing.Point(144, 258);
            this.labelDE.Name = "labelDE";
            this.labelDE.Size = new System.Drawing.Size(78, 13);
            this.labelDE.TabIndex = 37;
            this.labelDE.Text = "[DE] Dispenser";
            // 
            // comboBoxCP
            // 
            this.comboBoxCP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCP.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3"});
            this.comboBoxCP.Location = new System.Drawing.Point(12, 234);
            this.comboBoxCP.MaxDropDownItems = 4;
            this.comboBoxCP.Name = "comboBoxCP";
            this.comboBoxCP.Size = new System.Drawing.Size(31, 21);
            this.comboBoxCP.TabIndex = 29;
            this.comboBoxCP.SelectedIndexChanged += new System.EventHandler(this.comboBoxCP_SelectedIndexChanged);
            // 
            // GUIPetra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 282);
            this.Controls.Add(this.labelDE);
            this.Controls.Add(this.labelPP);
            this.Controls.Add(this.labelAP);
            this.Controls.Add(this.labelCS);
            this.Controls.Add(this.labelS);
            this.Controls.Add(this.labelT);
            this.Controls.Add(this.labelL2);
            this.Controls.Add(this.labelL1);
            this.Controls.Add(this.comboBoxCP);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.checkBoxGA);
            this.Controls.Add(this.checkBoxAA);
            this.Controls.Add(this.checkBoxPA);
            this.Controls.Add(this.checkBoxPV);
            this.Controls.Add(this.checkBoxC2);
            this.Controls.Add(this.checkBoxC1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonDisconnect);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.textBoxIP);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GUIPetra";
            this.Text = "PetraClient";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBoxC1;
        private System.Windows.Forms.CheckBox checkBoxC2;
        private System.Windows.Forms.CheckBox checkBoxPV;
        private System.Windows.Forms.CheckBox checkBoxPA;
        private System.Windows.Forms.CheckBox checkBoxAA;
        private System.Windows.Forms.CheckBox checkBoxGA;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelL1;
        private System.Windows.Forms.Label labelL2;
        private System.Windows.Forms.Label labelT;
        private System.Windows.Forms.Label labelS;
        private System.Windows.Forms.Label labelCS;
        private System.Windows.Forms.Label labelAP;
        private System.Windows.Forms.Label labelPP;
        private System.Windows.Forms.Label labelDE;
        private System.Windows.Forms.ComboBox comboBoxCP;
    }
}

