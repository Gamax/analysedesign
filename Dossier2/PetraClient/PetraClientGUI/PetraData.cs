﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Petra_client
{
    class PetraDataClass
    {
        public int captL1 = 0;
        public int captL2 = 0;
        public int captT = 0;
        public int captS = 0;
        public int captCS = 0;
        public int captAP = 0;
        public int captPP = 0;
        public int captDE = 0;

        public int actCP = 0; //0 à 3
        public int actC1 = 0;
        public int actC2 = 0;
        public int actPV = 0;
        public int actPA = 0;
        public int actAA = 0;
        public int actGA = 0;

        public String GetActuatorMessage()
        {
            return actCP.ToString() + actC1.ToString() + actC2.ToString() + actPV.ToString() + actPA.ToString() + actAA.ToString() + actGA.ToString() + '0';
        }

        public void SetCaptorFromMessage(String message)
        {
            captL1 = (int) Char.GetNumericValue(message[0]);
            captL2 = (int)Char.GetNumericValue(message[1]);
            captT = (int)Char.GetNumericValue(message[2]);
            captS = (int)Char.GetNumericValue(message[3]);
            captCS = (int)Char.GetNumericValue(message[4]);
            captAP = (int)Char.GetNumericValue(message[5]);
            captPP = (int)Char.GetNumericValue(message[6]);
            captDE = (int)Char.GetNumericValue(message[7]);
        }
    }
}
