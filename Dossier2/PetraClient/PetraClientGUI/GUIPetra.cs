﻿using PetraLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Petra_client
{
    public partial class GUIPetra : Form
    {
        private SocketClient socket;
        private bool isConnected = false;
        private PetraDataClass petraData;
        private Thread threadupdate;
        private bool runThread = false;

        public GUIPetra()
        {
            InitializeComponent();
            petraData = new PetraDataClass();
            comboBoxCP.SelectedIndex = 0;
            setStatus("Disconnected");
            
            threadupdate = new Thread(ThreadMethod);
            threadupdate.Start();

        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {

            if(isConnected == true)
            {
                MessageBox.Show("Already Connected to Petra", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            //Test de la présence de données dans les 2 champs
            if(textBoxIP.Text == "" || textBoxPort.Text == "")
            {
                MessageBox.Show("Please enter IP and Port", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            socket = new SocketClient();
            try
            {
                socket.Connect(textBoxIP.Text, Int32.Parse(textBoxPort.Text));
            }
            catch(SocketException error)
            {
                MessageBox.Show("Cannot connect to Petra : " + error.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            runThread = true;
            isConnected = true;
            setStatus("Connected");
            
        }



        private void checkBoxC1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxC1.Checked)
            {
                petraData.actC1 = 1;
            }
            else
            {
                petraData.actC1 = 0;
            }
        }

        private void checkBoxC2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxC2.Checked)
            {
                petraData.actC2 = 1;
            }
            else
            {
                petraData.actC2 = 0;
            }
        }

        private void checkBoxPV_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPV.Checked)
            {
                petraData.actPV = 1;
            }
            else
            {
                petraData.actPV = 0;
            }
        }

        private void checkBoxPA_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPA.Checked)
            {
                petraData.actPA = 1;
            }
            else
            {
                petraData.actPA = 0;
            }
        }

        private void checkBoxAA_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxAA.Checked)
            {
                petraData.actAA = 1;
            }
            else
            {
                petraData.actAA = 0;
            }
        }

        private void checkBoxGA_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxGA.Checked)
            {
                petraData.actGA = 1;
            }
            else
            {
                petraData.actGA = 0;
            }
        }

        private void comboBoxCP_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCP.SelectedIndex >= 0 || comboBoxCP.SelectedIndex < 4)
            {
                petraData.actCP = comboBoxCP.SelectedIndex;
            }
        }

        public void setStatus(String text)
        {
            StatusLabel.Text = text;
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            if(isConnected == false)
            {
                return;
            }

            //threadupdate.Abort();
            runThread = false;
            socket.Close();
            socket = null;
            isConnected = false;
            setStatus("Disconnected");
        }

        private void updatecapteur()
        {
            if(petraData.captL1 == 1)
            {
                labelL1.BackColor = Color.Green;
            }
            else
            {
                labelL1.BackColor = default(Color);
            }

            if (petraData.captL2 == 1)
            {
                labelL2.BackColor = Color.Green;
            }
            else
            {
                labelL2.BackColor = default(Color);
            }

            if (petraData.captT == 1)
            {
                labelT.BackColor = Color.Green;
            }
            else
            {
                labelT.BackColor = default(Color);
            }

            if (petraData.captS == 1)
            {
                labelS.BackColor = Color.Green;
            }
            else
            {
                labelS.BackColor = default(Color);
            }

            if (petraData.captCS == 1)
            {
                labelCS.BackColor = Color.Green;
            }
            else
            {
                labelCS.BackColor = default(Color);
            }

            if (petraData.captAP == 1)
            {
                labelAP.BackColor = Color.Green;
            }
            else
            {
                labelAP.BackColor = default(Color);
            }

            if (petraData.captPP == 1)
            {
                labelPP.BackColor = Color.Green;
            }
            else
            {
                labelPP.BackColor = default(Color);
            }

            if (petraData.captDE == 1)
            {
                labelDE.BackColor = Color.Green;
            }
            else
            {
                labelDE.BackColor = default(Color);
            }
        }

        private void ThreadMethod()
        {
            while (true)
            {

                if (runThread)
                {
                    while (runThread)
                    {
                        try
                        {
                            socket.Send(petraData.GetActuatorMessage());
                            petraData.SetCaptorFromMessage(socket.Receive());
                        }
                        catch(SocketException error)
                        {
                            isConnected = false;
                            socket.Close();
                            return;
                        }

                        updatecapteur();

                        Thread.Sleep(100); //todo change
                    }
                }
                else
                    Thread.Sleep(2000);
            }
        }
    }
}
