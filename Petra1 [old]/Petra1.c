#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>

typedef struct {
	unsigned CP : 2; //Chariot
	unsigned C1 : 1; //Conv1
	unsigned C2 : 1; //Conv2
	unsigned PV : 1; //Ventouse
	unsigned PA : 1; //Plongeur
	unsigned AA : 1; //Arbre
	unsigned GA : 1; //Grapin sur Arbre
} struct_actuators;

typedef union {
	struct_actuators actuators;
	unsigned char byte;
} struct_u_actuators;

typedef struct {
	unsigned L1 : 1; //Capteur de corner 1 (trou à 0)
	unsigned L2 : 1; //Capteur de corner 2 (trou à 0)
	unsigned T  : 1; /* cablé H */
	unsigned S  : 1; //Capteur de slot (trou à 0)
	unsigned CS : 1; //Capteur de chariot stable (immobile à 0)
	unsigned AP : 1; //Capteur de position de l'arbre (Tapis 1 à 0)
	unsigned PP : 1; //Capteur de position du plongeur (haut à 0)
	unsigned DE : 1; //Capteur du dispenser (vide à 1)
} struct_sensors;

typedef union {
	struct_sensors sensors;
	unsigned char byte;
} struct_u_sensors;

typedef struct {
	struct_u_actuators actuators_u;
	struct_u_sensors sensors_u;
	int fd_petra_in;
	int fd_petra_out;
} struct_petra;

void displaySensors(struct_petra *petra);
void displayActuators(struct_petra *petra);

void MenuActu();
void Sleep(int,int);
void Monitoring(struct_petra *);

int main(int argc, char *argv[])
{
	int ret_menuacu;
	int monitor;

	struct_petra petra;

	petra.fd_petra_out = open("/dev/actuateursPETRA", O_WRONLY);
	if ( petra.fd_petra_out == -1 ){
		printf("Erreur initialisation actuateurs petra\n");
		return -1;
	}

	petra.fd_petra_in = open ( "/dev/capteursPETRA", O_RDONLY );
	if ( petra.fd_petra_in == -1 ){
		printf("Erreur initialisation capteurs petra\n");
		return -1;
	}


	petra.actuators_u.byte = 0;
	petra.sensors_u.byte = 0;


	//on clean les fichier du petra
	read(petra.fd_petra_in, &petra.sensors_u.byte, 1);
	write(petra.fd_petra_out, &petra.actuators_u.byte, 1);

	//Lancement du processus de monitoring
	monitor = fork();
	if(monitor == 0)
	{
		Monitoring(&petra);
	}

	printf("Monitor process pid : %d\n",monitor);

	displaySensors(&petra);
	displayActuators(&petra);
	MenuActu();

	do //Boucle tant que le choix n'est pas = 11
	{
		system("clear");
		displaySensors(&petra);
		displayActuators(&petra);
		MenuActu();
		fflush(stdin);
		scanf("%d",&ret_menuacu);

		switch(ret_menuacu)
		{
			case 1 : //Chariot 0
				petra.actuators_u.actuators.CP = 0;
				break;
			case 2 : //Chariot 1
				petra.actuators_u.actuators.CP = 1;
				break;
			case 3 : //Chariot 2
				petra.actuators_u.actuators.CP = 2;
				break;
			case 4 : //Chariot 3
				petra.actuators_u.actuators.CP = 3;
				break;
			case 5 : //Convoyeur 1
				petra.actuators_u.actuators.C1 = !petra.actuators_u.actuators.C1;
				break;
			case 6 : //Convoyeur 2
				petra.actuators_u.actuators.C2 = !petra.actuators_u.actuators.C2;
				break;
			case 7: //Ventouse
				petra.actuators_u.actuators.PV = !petra.actuators_u.actuators.PV;
				break;
			case 8 : //Plongeur
				petra.actuators_u.actuators.PA = !petra.actuators_u.actuators.PA;
				break;
			case 9 : //Arbre
				petra.actuators_u.actuators.AA = !petra.actuators_u.actuators.AA;
				break;
			case 10 : //Grapin
				petra.actuators_u.actuators.GA = !petra.actuators_u.actuators.GA;
				break;
			case 11 :
				printf("Goodbye");
				break;
			default : break;
		}

		if(ret_menuacu != 11)
		    write(petra.fd_petra_out, &petra.actuators_u.byte, 1);

	}while(ret_menuacu != 11);

	//Fermeture des fichiers
	close(petra.fd_petra_in);
	close(petra.fd_petra_out);

	return 0;
}
void MenuActu()
{

	printf("\nActuateur a activer : \n");
	printf("1 - Chariot 0\n");
	printf("2 - Chariot 1\n");
	printf("3 - Chariot 2\n");
	printf("4 - Chariot 3\n");
	printf("5 - Convoyeur 1\n");
	printf("6 - Convoyeur 2\n");
	printf("7 - Ventouse\n");
	printf("8 - Plongeur sur chariot\n");
	printf("9 - Arbre\n");
	printf("10 - Grapin\n");
	printf("11 - Quittez\n>");
}
void Sleep(int sec, int nsec)
{
	struct timespec time_id;

	time_id.tv_sec= sec;
	time_id.tv_nsec = nsec;

	nanosleep(&time_id,NULL);
}
//Processus Monitoring
void Monitoring(struct_petra *petra)
{
	struct_u_sensors first,second;

	//On lit (stocke) l'�tat initial
	read(petra->fd_petra_in,&first.byte,1);

	while(1)
	{
		Sleep(0,100000000); //100 ms

		//On lit l'�tat, puis on le compare au dernier lu
		read(petra->fd_petra_in,&second.byte,1);

		if(first.byte != second.byte) //Il y a eu un changement !
		{
			system("clear");

			//On r�-affiche les capteurs + menu en dessous
			displaySensors(petra);
			displayActuators(petra);
			MenuActu();

			first.byte = second.byte; //On sauve le dernier �tat lu !
		}
		//Si il n'y pas de changement, pas de soucis
	}
}

void displaySensors(struct_petra *petra){

	if(petra == NULL)
	{
		perror("ERROR : Couldn't connect to PETRA\n");
		return;
	}

	read(petra->fd_petra_in, &petra->sensors_u.byte, 1);

	printf("+----------------------+\n"
		   "| PETRA SENSORS STATES |\n"
		   "+----------------------+\n\n");

	printf("  L1 : %u    L2 : %u\n",petra->sensors_u.sensors.L1,petra->sensors_u.sensors.L2);
	printf("  T  : %u    S  : %u\n",petra->sensors_u.sensors.T,petra->sensors_u.sensors.S);
	printf("  CS : %u    AP : %u\n",petra->sensors_u.sensors.CS,petra->sensors_u.sensors.AP);
	printf("  PP : %u    DE : %u\n",petra->sensors_u.sensors.PP,petra->sensors_u.sensors.DE);
}

void displayActuators(struct_petra *petra){

	if(petra == NULL)
	{
		perror("ERROR : Couldn't connect to PETRA\n");
		return;
	}

	printf("+------------------------+\n"
		   "| PETRA ACTUATORS STATES |\n"
		   "+------------------------+\n\n");

	printf("  CP : %u    C1 : %u\n",petra->actuators_u.actuators.CP,petra->actuators_u.actuators.C1);
	printf("  C2 : %u    PV : %u\n",petra->actuators_u.actuators.C2,petra->actuators_u.actuators.PV);
	printf("  PA : %u    AA : %u\n",petra->actuators_u.actuators.PA,petra->actuators_u.actuators.AA);
	printf("  GA : %u\n",petra->actuators_u.actuators.GA);
}
